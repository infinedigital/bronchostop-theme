<?php
// Generic text fields
$generic = get_field( 'generic', 'options' );

// Fields
?>

<section>
	<article class="container">
		<div class="row">
			<div class="col">
				<h1>
					<?php the_title(); ?>
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<picture>
					<?php the_post_thumbnail(); ?>
				</picture>
			</div>
		</div>
		<?php  while ( have_rows( 'content' ) ) : the_row(); ?>
		<div class="row">
			<?php
			if( get_row_layout() == 'one_column' ):
				$text = get_sub_field( 'text' );
			?>
			<div class="col wysiwyg">
				<?php echo $text; ?>
			</div>
			<?php
			elseif( get_row_layout() == 'left_image' ):
				$text = get_sub_field( 'text' );
				$img =  get_sub_field( 'image' );
				$alt    = $img[ 'alt' ];
				$large  = $img[ 'sizes' ][ 'large' ];
				$small  = $img[ 'sizes' ][ 'large' ];
				$height = $img[ 'sizes' ][ 'large-height' ];
				$width  = $img[ 'sizes' ][ 'large-width' ];
			?>
			<div class="col">
				<picture>
					<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
					<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
				</picture>
			</div>
			<div class="col wysiwyg">
				<?php echo $text; ?>
			</div>
			<?php
			elseif( get_row_layout() == 'right_image' ):
				$text = get_sub_field( 'text' );
				$img =  get_sub_field( 'image' );
				$alt    = $img[ 'alt' ];
				$large  = $img[ 'sizes' ][ 'large' ];
				$small  = $img[ 'sizes' ][ 'large' ];
				$height = $img[ 'sizes' ][ 'large-height' ];
				$width  = $img[ 'sizes' ][ 'large-width' ];
			?>
			<div class="col wysiwyg">
				<?php echo $text; ?>
			</div>
			<div class="col">
				<picture>
					<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
					<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
				</picture>
			</div>
			<?php endif; ?>
		</div>
		<?php endwhile; ?>
		<div class="row">
			<div class="col">
				<a href="" title="<?php echo $generic[ 'all_tips' ]; ?>">
					<?php echo $generic[ 'all_tips' ]; ?>
				</a>
			</div>
			<div class="col">
				<a href="" title="<?php echo $generic[ 'tips_next' ]; ?>">
					<?php echo $generic[ 'tips_next' ]; ?> 
				</a>
			</div>
		</div>
	</article>
</section>


<section>
	
</section>