<?php
/*
* Template Name: Products
*/

// Generic text fields
$generic = get_field( 'generic', 'options' );

// Fields
$selected   = get_field( 'selection' );

// Categories for filters
$term    = get_queried_object();
$term_id = ( isset( $term->term_id ) ) ? (int) $term->term_id : 0;
$categories = get_categories( array(
	'taxonomy'   => 'product-category',
	'orderby'    => 'name',
	'parent'     => 0,
	'hide_empty' => 1,
) );

$modal = [];
$e     = 0;
?>

<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<h1><?php the_title(); ?></h1>
				<ul>
					<li>
						<button class="btn btn-outline" data-filter="all"><?php echo $generic[ 'all_products' ]; ?></button>
					</li>
					<?php 
					foreach ( $categories as $category ) :
						$cat_ID   = (int) $category->term_id;
        		$cat_name = $category->name;
        		$cat_link = get_category_link( $category->term_id );
					?> 
					<li>
						<span class="btn btn-outline" data-filter="<?php echo strtolower( $cat_name ) ?>">
							<?php echo $cat_name; ?> 
						</span>
					</li>
					<?php endforeach; ?> 
				</ul>
			</div>
		</div>
		<div class="row">
			<?php
			foreach( $selected as $post ) :
				setup_postdata( $post );
				$pack    = get_field( 'pack' );
				$product = get_field( 'product' );
			?> 
			<div class="col" data-terms="<?php $terms = get_terms( 'product-category', array( 'hide_empty' => 0, 'parent' =>0 ) ); foreach( $terms as $term ) : $name = $term->name; echo strtolower( $name.' ' );  endforeach; ?> ">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<div>
						<?php the_post_thumbnail( 'large' ); ?>
					</div>
					<div>
						<ul>
							<?php 
							foreach( $terms as $term ) :
								$link = get_term_link( $term->slug, $term->taxonomy );
								$name = $term->name;
							?> 
							<li>
								<?php echo $name; ?> 
							</li>
							<?php endforeach; ?> 
						</ul>
						<h5><?php the_title(); ?></h5>
						<div>
							<?php echo $product[ 'desc-short' ]; ?> 
						</div>
					</div>
				</a>
			</div>
			<?php 
			endforeach; 
			wp_reset_postdata();
			?> 
		</div>
	</div>
</section>