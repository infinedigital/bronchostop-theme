<?php
/*
* Template Name: Why
*/

// Generic text fields
$generic = get_field( 'generic', 'options' );

// Fields
$first   = get_field( 'first' );
$second  = get_field( 'second' );
$third   = get_field( 'third' );
$forth   = get_field( 'forth' );
$fifth   = get_field( 'fifth' );

$modal = [];
$e     = 0;
?>

<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<?php 
				if( $first[ 'image' ] ) : 
					$img    = $first[ 'image' ];
					$alt    = $img[ 'alt' ];
					$large  = $img[ 'sizes' ][ 'large' ];
					$small  = $img[ 'sizes' ][ 'large' ];
					$height = $img[ 'sizes' ][ 'large-height' ];
					$width  = $img[ 'sizes' ][ 'large-width' ];
				?>
				<picture>
					<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
					<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
				</picture>
				<?php endif; ?>
			</div>
			<div class="col" data-floating="<?php $i = 1; echo $i; $i++; ?>">
				<h2><?php echo $first[ 'content' ][ 'title' ]; ?></h2>
				<div class="custom-wysisyg">
					<?php echo $first[ 'content' ][ 'text' ]; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col" data-floating="<?php echo $i; $i++; ?>">
				<h2><?php echo $second[ 'content' ][ 'title' ]; ?></h2>
				<div class="custom-wysisyg">
					<?php echo $second[ 'content' ][ 'text' ]; ?>
				</div>
				<?php
				if( have_rows( 'second' ) ) : 
					while( have_rows( 'second' ) ) : the_row();
				?> 
				<ul class="inline-list">
					<?php
					if( have_rows( 'icon-list' ) ) : 
						while( have_rows( 'icon-list' ) ) : the_row();
							$icon = get_sub_field( 'icon' );
					?> 
					<li>
						<picture>
							<img src="<?php echo $icon[ 'url' ]; ?>" alt="<?php echo $icon[ 'title' ]; ?>" />
						</picture>
					</li>
					<?php 
						endwhile; 
					endif; 
					?> 
				</ul>
				<?php 
					endwhile; 
				endif; 
				?> 
			</div>
			<div class="col">
				<?php 
				if( $third[ 'image' ] ) : 
					$img    = $third[ 'image' ];
					$alt    = $img[ 'alt' ];
					$large  = $img[ 'sizes' ][ 'large' ];
					$small  = $img[ 'sizes' ][ 'large' ];
					$height = $img[ 'sizes' ][ 'large-height' ];
					$width  = $img[ 'sizes' ][ 'large-width' ];
				?> 
				<picture>
					<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
					<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
				</picture>
				<?php endif; ?> 
			</div>
			<div class="col" data-floating="<?php echo $i; $i++; ?>">
				<h2><?php echo $third[ 'content' ][ 'title' ]; ?></h2>
				<div class="custom-wysisyg">
					<?php echo $third[ 'content' ][ 'text' ]; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col" data-floating="<?php echo $i; $i++; ?>">
				<h2><?php echo $forth[ 'content' ][ 'title' ]; ?></h2>
				<div class="custom-wysisyg">
					<?php echo $forth[ 'content' ][ 'text' ]; ?>
				</div>
				<a href="<?php echo $forth[ 'content' ][ 'btn' ][ 'url' ]; ?>" title="<?php echo $forth[ 'content' ][ 'btn' ][ 'title' ]; ?>">
					<?php echo $forth[ 'content' ][ 'btn' ][ 'title' ]; ?> 
				</a>
			</div>
			<div class="col">
				<?php 
				if( $forth[ 'images' ][ 'image-1' ] ) : 
					$img    = $forth[ 'images' ][ 'image-1' ];
					$alt    = $img[ 'alt' ];
					$large  = $img[ 'sizes' ][ 'large' ];
					$small  = $img[ 'sizes' ][ 'large' ];
					$height = $img[ 'sizes' ][ 'large-height' ];
					$width  = $img[ 'sizes' ][ 'large-width' ];
				?>
				<picture>
					<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
					<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
				</picture>
				<?php endif; ?>
				<?php 
				if( $forth[ 'images' ][ 'image-2' ] ) : 
					$img    = $forth[ 'images' ][ 'image-2' ];
					$alt    = $img[ 'alt' ];
					$large  = $img[ 'sizes' ][ 'large' ];
					$small  = $img[ 'sizes' ][ 'large' ];
					$height = $img[ 'sizes' ][ 'large-height' ];
					$width  = $img[ 'sizes' ][ 'large-width' ];
				?>
				<picture>
					<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
					<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
				</picture>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col">
				<?php 
				if( $fifth[ 'image' ] ) : 
					$img    = $fifth[ 'image' ];
					$alt    = $img[ 'alt' ];
					$large  = $img[ 'sizes' ][ 'large' ];
					$small  = $img[ 'sizes' ][ 'large' ];
					$height = $img[ 'sizes' ][ 'large-height' ];
					$width  = $img[ 'sizes' ][ 'large-width' ];
				?>
				<picture>
					<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
					<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
				</picture>
				<?php endif; ?>
			</div>
			<div class="col" data-floating="<?php echo $i; $i++; ?>">
				<h2><?php echo $fifth[ 'content' ][ 'title' ]; ?></h2>
				<div class="custom-wysisyg">
					<?php echo $fifth[ 'content' ][ 'text' ]; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_template_part( 'templates/facts' ); ?> 