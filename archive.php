<?php
// Generic text fields
$generic = get_field( 'generic', 'options' );

// Argument for wordpress loop
$args = array(
	'post_type'      => 'post',
	'post_status'    => 'publish',
	'orderby'        => 'ID',
	'posts_per_page' => '3',
);

$modal = [];
$e     = 0;
?>

<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<h1><?php echo $title; ?></h1>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<?php
		$tips = new WP_Query( $args );
		if ( $tips->have_posts() ) :
		?>
		<div class="row">
			<?php while ( $tips->have_posts() ) : $tips->the_post(); ?>
			<article class="col post">
				<header class="post-header">
					<a href="<?php the_permalink() ?>" class="post-thumbnail" title="<?php echo $generic[ 'read_more' ]; ?>">
						<picture class="post-img">
							<?php the_post_thumbnail(); ?>
						</picture>
					</a>
				</header>
				<main class="post-main">
					<h2 class="post-title">
						<?php the_title(); ?> 
					</h2>
					<div class="post-exerpt">
						<?php the_excerpt(); ?> 
					</div>
				</main>
				<footer class="post-footer">
					<a href="<?php the_permalink() ?>" class="post-link" title="<?php echo $generic[ 'read_more' ]; ?>">
						<?php echo $generic[ 'read_more' ]; ?> 
					</a>
				</footer>
			</article>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
</section>