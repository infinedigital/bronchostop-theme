const mapStyle = [{
    'featureType': 'administrative',
    'elementType': 'all',
    'stylers': [{
        'visibility': 'on',
    },
        {
            'lightness': 33,
        },
    ],
},
    {
        'featureType': 'landscape',
        'elementType': 'all',
        'stylers': [{
            'color': '#f5f5f5',
        }],
    },
    {
        'featureType': 'poi.park',
        'elementType': 'geometry',
        'stylers': [{
            'color': '#b6e1b6',
        }],
    },
    {
        'featureType': 'poi.park',
        'elementType': 'labels',
        'stylers': [{
            'visibility': 'on',
        },
            {
                'lightness': 20,
            },
        ],
    },
    {
        'featureType': 'road',
        'elementType': 'all',
        'stylers': [{
            'lightness': 20,
        }],
    },
    {
        'featureType': 'road.highway',
        'elementType': 'geometry',
        'stylers': [{
            'color': 'red',
        }],
    },
    {
        'featureType': 'road.arterial',
        'elementType': 'geometry',
        'stylers': [{
            'color': '#ffffff',
        }],
    },
    {
        'featureType': 'road.local',
        'elementType': 'geometry',
        'stylers': [{
            'color': '#fbfaf7',
        }],
    },
    {
        'featureType': 'water',
        'elementType': 'all',
        'stylers': [{
            'visibility': 'on',
        },
            {
                'color': '#abdaff',
            },
        ],
    },
];

function initMap() {

    var dataStore = document.getElementById('jsStore');

    const map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: {lat: 50.85045, lng: 4.34878},
        styles: mapStyle,
        mapTypeControl: false
    });
    map.data.loadGeoJson("./themes/"+dataStore.dataset.theme+"/assets/json/store.json", {idPropertyName: 'storeid'});
    // map.data.setStyle((feature) => {
    //     return {
    //         icon: {
    //             url: `img/icon_${feature.getProperty('category')}.png`,
    //             scaledSize: new google.maps.Size(64, 64),
    //         },
    //     };
    // });

    const infoWindow = new google.maps.InfoWindow();
    map.data.addListener('click', (event) => {
        const category = event.feature.getProperty('category');
        const name = event.feature.getProperty('name');
        const description = event.feature.getProperty('description');
        const hours = event.feature.getProperty('hours');
        const phone = event.feature.getProperty('phone');
        const adress = event.feature.getProperty('adress');
        const position = event.feature.getGeometry().get();
        const content = `
      <div style="margin: 10px;">
        <p class="place">${name}</p>
        <p>${adress}</p>
        <p>${phone}</p>
      </div>
      `;

        infoWindow.setContent(content);
        infoWindow.setPosition(position);
        infoWindow.setOptions({pixelOffset: new google.maps.Size(0, -30)});
        infoWindow.open(map);
    });

    const card = document.createElement('div');
    const titleBar = document.createElement('div');
    const container = document.createElement('div');
    const input = document.createElement('input');
    const options = {
        types: ['address'],
    };

    card.setAttribute('id', 'pac-card');
    container.setAttribute('id', 'pac-container');
    input.setAttribute('id', 'pac-input');
    input.setAttribute('type', 'text');
    input.setAttribute('placeholder', 'Enter an address');
    container.appendChild(input);
    card.appendChild(titleBar);
    card.appendChild(container);
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

    const autocomplete = new google.maps.places.Autocomplete(input, options);

    autocomplete.bindTo('bounds', map);

    autocomplete.setFields(
        ['address_components', 'geometry', 'name']);

    const originMarker = new google.maps.Marker({map: map});
    originMarker.setVisible(false);
    let originLocation = map.getCenter();

    autocomplete.addListener('place_changed', async () => {
        originMarker.setVisible(false);
        originLocation = map.getCenter();
        const place = autocomplete.getPlace();

        if (!place.geometry) {
            window.alert('No address available for input: \'' + place.name + '\'');
            return;
        }

        originLocation = place.geometry.location;
        map.setCenter(originLocation);
        map.setZoom(13);

        originMarker.setPosition(originLocation);
        originMarker.setVisible(true);

        const rankedStores = await calculateDistances(map.data, originLocation);
        showStoresList(map.data, rankedStores);

        return;
    });
}
async function calculateDistances(data, origin) {
    const stores = [];
    const destinations = [];

    data.forEach((store) => {
        const storeNum = store.getProperty('storeid');
        const storeLoc = store.getGeometry().get();

        stores.push(storeNum);
        destinations.push(storeLoc);
    });

    const service = new google.maps.DistanceMatrixService();
    const getDistanceMatrix =
        (service, parameters) => new Promise((resolve, reject) => {
            service.getDistanceMatrix(parameters, (response, status) => {
                if (status != google.maps.DistanceMatrixStatus.OK) {
                    reject(response);
                } else {
                    const distances = [];
                    const results = response.rows[0].elements;
                    for (let j = 0; j < results.length; j++) {
                        const element = results[j];
                        const distanceText = element.distance.text;
                        const distanceDuration = element.duration.text;
                        const distanceVal = element.distance.value;
                        const distanceObject = {
                            storeid: stores[j],
                            distanceText: distanceText,
                            distanceDuration: distanceDuration,
                            distanceVal: distanceVal,
                        };
                        distances.push(distanceObject);
                    }

                    resolve(distances);
                }
            });
        });
    const distancesList = await getDistanceMatrix(service, {
        origins: [origin],
        destinations: destinations,
        travelMode: 'DRIVING',
        unitSystem: google.maps.UnitSystem.METRIC,
    });
    distancesList.sort((first, second) => {
        return first.distanceVal - second.distanceVal;
    });

    return distancesList;
}

function showStoresList(data, stores) {
    if (stores.length == 0) {
        console.log('empty stores');
        return;
    }

    let panel = document.createElement('div');
    // If the panel already exists, use it. Else, create it and add to the page.
    if (document.getElementById('panel')) {
        panel = document.getElementById('panel');
        // If panel is already open, close it
        if (panel.classList.contains('open')) {
            panel.classList.remove('open');
        }
    } else {
        panel.setAttribute('id', 'panel');
        const body = document.getElementById('map');
        body.insertBefore(panel, body.childNodes[0]);
    }

    while (panel.lastChild) {
        panel.removeChild(panel.lastChild);
    }

    stores.forEach((store) => {
        const onestore = document.createElement('div');
        onestore.classList.add('onestore');
        const name = document.createElement('p');
        name.classList.add('place');
        const currentStore = data.getFeatureById(store.storeid);
        const address = document.createElement('p');
        address.textContent = currentStore.getProperty('adress');
        name.textContent = currentStore.getProperty('name');
        onestore.appendChild(name);
        onestore.appendChild(address);
        const distanceDuration = document.createElement('p');
        distanceDuration.classList.add('distanceText');
        distanceDuration.textContent = store.distanceDuration;
        onestore.appendChild(distanceDuration);
        const distanceText = document.createElement('p');
        distanceText.classList.add('distanceText');
        distanceText.textContent = store.distanceText;
        onestore.appendChild(distanceText);
        panel.appendChild(onestore);
    });
    panel.classList.add('open');

    return;
}
