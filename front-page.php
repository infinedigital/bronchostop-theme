<?php
// Generic text fields
$generic = get_field( 'generic', 'options' );

// Fields
$brand    = get_field( 'brand', 'options' );
$landing  = get_field( 'landing' );
$products = get_field( 'products' );
$why      = get_field( 'why' );
$tips     = get_field( 'tips' );
?>

<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<h1>
					<span><?php echo $brand[ 'name' ]; ?></span>
					<picture>
						<img src="<?php echo $brand[ 'logo' ][ 'url' ]; ?>" alt="<?php echo $brand[ 'name' ]; ?>">
					</picture>
				</h1>
				<h2>
					<?php echo $brand[ 'slogan' ]; ?>
				</h2>
				<h3>
					<?php echo $landing[ 'content' ][ 'text' ]; ?>
				</h3>
				<?php 
				if( $landing[ 'content' ][ 'btn' ] ) :
					$link = $landing[ 'content' ][ 'btn' ];
				?>
				<a href="<?php echo $link[ 'url' ]; ?>" class="btn" title="<?php echo $link[ 'title' ]; ?>">
					<?php echo $link[ 'title' ]; ?> 
				</a>
				<?php endif; ?>
			</div>
			<div class="col">
				<?php 
				if( $landing[ 'image' ] ) : 
					$img    = $landing[ 'image' ];
					$alt    = $img[ 'alt' ];
					$large  = $img[ 'sizes' ][ 'large' ];
					$small  = $img[ 'sizes' ][ 'large' ];
					$height = $img[ 'sizes' ][ 'large-height' ];
					$width  = $img[ 'sizes' ][ 'large-width' ];
				?>
				<picture>
					<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
					<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
				</picture>
				<?php endif; ?>
			</div>
		</div>
		<?php 
		if( $landing[ 'video' ][ 'display' ] ) :
			$video   = $landing[ 'video' ];
			$content = $video[ 'content' ];
		?>
		<a href="<?php echo $video[ 'link' ]; ?>" class="" title="<?php echo $content[ 'text' ]; ?>">
			<div>
				<div>
					<?php echo $content[ 'title' ]; ?> 
				</div>
				<div>
					<?php echo $content[ 'text' ]; ?> 
				</div>
			</div>
			<?php 
			if( $video[ 'image' ] ) : 
				$img    = $video[ 'image' ];
				$alt    = $img[ 'alt' ];
				$small  = $img[ 'sizes' ][ 'medium' ];
				$height = $img[ 'sizes' ][ 'large-height' ];
				$width  = $img[ 'sizes' ][ 'large-width' ];
			?>
			<picture>
				<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
			</picture>
			<?php endif; ?>
		</a>
		<?php endif; ?>
	</div>
</section>

<?php if( $products[ 'display' ] == 'two' ) : ?>
<section>
	<div class="container">
		<?php if( $products[ 'title' ] ) : ?>
		<div class="row">
			<div class="col text-center">
				<?php echo $products[ 'title' ]; ?>
			</div>
		</div>
		<?php endif; ?>
		<div class="row">
			<?php if( $products[ 'left' ] ) : 
				$title = $products[ 'left' ][ 'title' ];
				$link  = $products[ 'left' ][ 'link' ];
				$img   = $products[ 'left' ][ 'image' ];
			?>
			<div class="col">
				<div>
					<h2>
						<?php echo $title; ?> 
					</h2>
					<a href="<?php echo $link[ 'url' ]; ?>" class="btn" title="<?php echo $link[ 'title' ]; ?>">
						<?php echo $link[ 'title' ]; ?> 
					</a>
					<?php 
					if( $img ) : 
						$alt    = $img[ 'alt' ];
						$large  = $img[ 'sizes' ][ 'large' ];
						$small  = $img[ 'sizes' ][ 'large' ];
						$height = $img[ 'sizes' ][ 'large-height' ];
						$width  = $img[ 'sizes' ][ 'large-width' ];
					?>
					<picture>
						<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
						<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
					</picture>
					<?php endif; ?>
				</div>
			</div>
			<?php endif; ?>
			<?php if( $products[ 'right' ] ) : 
				$title = $products[ 'right' ][ 'title' ];
				$link  = $products[ 'right' ][ 'link' ];
				$img   = $products[ 'right' ][ 'image' ];
			?>
			<div class="col">
				<div>
					<h2>
						<?php echo $title; ?> 
					</h2>
					<a href="<?php echo $link[ 'url' ]; ?>" class="btn" title="<?php echo $link[ 'title' ]; ?>">
						<?php echo $link[ 'title' ]; ?> 
					</a>
					<?php 
					if( $img ) : 
						$alt    = $img[ 'alt' ];
						$large  = $img[ 'sizes' ][ 'large' ];
						$small  = $img[ 'sizes' ][ 'large' ];
						$height = $img[ 'sizes' ][ 'large-height' ];
						$width  = $img[ 'sizes' ][ 'large-width' ];
					?>
					<picture>
						<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
						<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
					</picture>
					<?php endif; ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>

<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<?php 
				if( $why[ 'images' ][ 'image-1' ] ) : 
					$img    = $why[ 'images' ][ 'image-1' ];
					$alt    = $img[ 'alt' ];
					$large  = $img[ 'sizes' ][ 'large' ];
					$small  = $img[ 'sizes' ][ 'large' ];
					$height = $img[ 'sizes' ][ 'large-height' ];
					$width  = $img[ 'sizes' ][ 'large-width' ];
				?>
				<picture>
					<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
					<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
				</picture>
				<?php endif; ?>
				<?php 
				if( $why[ 'images' ][ 'image-2' ] ) : 
					$img    = $why[ 'images' ][ 'image-2' ];
					$alt    = $img[ 'alt' ];
					$large  = $img[ 'sizes' ][ 'large' ];
					$small  = $img[ 'sizes' ][ 'large' ];
					$height = $img[ 'sizes' ][ 'large-height' ];
					$width  = $img[ 'sizes' ][ 'large-width' ];
				?>
				<picture>
					<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
					<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
				</picture>
				<?php endif; ?>
			</div>
			<div class="col">
				<?php if( $why[ 'content' ] ) : ?>
				<h3><?php echo $why[ 'content' ][ 'title' ]; ?></h3>
				<div>
					<?php echo $why[ 'content' ][ 'text' ]; ?>
				</div>
				<?php
				if( $why[ 'content' ][ 'btn' ] ) : 
					$link = $why[ 'content' ][ 'btn' ];
				?>
				<a href="<?php echo $link[ 'url' ]; ?>" class="btn" title="<?php echo $link[ 'title' ]; ?>">
					<?php echo $link[ 'title' ]; ?> 
				</a>
				<?php endif; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<?php if( $tips[ 'selected' ] ) : ?>
<section>
	<div class="container">
		<div class="row">
			<h4 class="col">
				<?php echo $generic[ 'tips_cough' ]; ?>
			</h4>
			<div class="col">
				<a href="<?php echo $tips[ 'link' ][ 'url' ]; ?>" title="<?php echo $tips[ 'link' ][ 'title' ]; ?>">
					<?php echo $tips[ 'link' ][ 'title' ]; ?> 
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col">
			<?php
			foreach( $tips[ 'selected' ] as $post ) :
				setup_postdata( $post );
				$excerpt = get_field( 'excerpt' );
			?> 
			<article class="col post">
				<header class="post-header">
					<a href="<?php the_permalink() ?>" class="post-thumbnail" title="<?php echo $generic[ 'read_more' ]; ?>">
						<picture class="post-img">
							<?php the_post_thumbnail(); ?>
						</picture>
					</a>
				</header>
				<main class="post-main">
					<h2 class="post-title">
						<?php the_title(); ?> 
					</h2>
					<div class="post-exerpt">
						<?php echo $excerpt; ?> 
					</div>
				</main>
				<footer class="post-footer">
					<a href="<?php the_permalink() ?>" class="post-link" title="<?php echo $generic[ 'read_more' ]; ?>">
						<?php echo $generic[ 'read_more' ]; ?> 
					</a>
				</footer>
			</article>
			<?php 
			endforeach; 
			wp_reset_postdata();
			?> 
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php get_template_part( 'templates/facts' ); ?> 

<!--IE11-Popup-->
<script>
	var $buoop = { required:{e:-1}, mobile:false, style:"bottom", api:2020.07 };
	
	function $buo_f(){
			var e = document.createElement("script");
			e.src = "//browser-update.org/update.min.js";
			document.body.appendChild(e);
	};
	
	try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
	
	catch(e){window.attachEvent("onload", $buo_f)}
</script>