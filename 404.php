<?php
	$title    = get_field( 'title_404', 'options' );
	$sub      = get_field( 'subtitle_404', 'options' );
	$desc     = get_field( 'desc_404', 'options' );
	$link_one = get_field( 'first_link_404', 'options' );
	$link_two = get_field( 'second_link_404', 'options' );
?>

<section class="generic-banner page-404">
	<div class="container text-center">
		<h1 class="big-title max-width-sm centered">
			<?php echo $title; ?>
		</h1>
		<h3 class="">
			<?php echo $sub; ?>
		</h3>
		<p class="max-width-md centered">
			<?php echo $desc; ?>
		</p>
		<ul class="list-inline pt-4">
			<?php if ( $link_one ) : ?>
			<li class="list-inline-item">
				<a href="<?php echo $link_one['url']; ?>" class="btn-primary" title="<?php echo $link_one['title']; ?>">
					<?php echo $link_one['title']; ?>
				</a>
			</li>
			<?php endif; ?>
			<?php if ( $link_two ) : ?>
			<li class="list-inline-item">
				<a href="<?php echo $link_two['url']; ?>" class="btn-secondary" title="<?php echo $link_two['title']; ?>">
					<?php echo $link_two['title']; ?>
				</a>
			</li>
			<?php endif; ?>
		</ul>
	</div>
</section>
