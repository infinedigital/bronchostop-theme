<?php
/*
* Template Name: Buy Now
*/

// Generic text fields
$generic = get_field( 'generic', 'options' );

// Fields
$title         = get_field( 'title' );
$locator_label = get_field('locator-label');
$locator_btn   = get_field('locator-btn');

$modal = [];
$e     = 0;
?>

<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<h1><?php echo $title; ?></h1>
			</div>
		</div>
	</div>
</section>

<?php if( have_rows( 'retailers', 'options' ) ): ?>
<section class="retailers">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php
				while ( have_rows( 'retailers', 'options' ) ) : the_row();
				$logo = get_sub_field( 'logo' );
				$link = get_sub_field( 'link' );
				$id   = get_sub_field( 'id' );
				?> 
				<a id="<?php echo $id; ?>" href="<?php echo $link; ?>" class="" title="<?php echo $generic[ 'buy_now' ]; ?>" target="_blank">
					<img src="<?php echo $logo[ 'url' ]; ?>" class="" alt="<?php echo $logo[ 'title' ]; ?>">
					<button class="btn btn-outline">
						<?php echo $generic[ 'buy_now' ]; ?> 
					</button>
				</a>
			<?php endwhile;  ?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?> 

<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<input type="text" placeholder="<?php echo $locator_label; ?>">
				<button class="btn">
					<?php echo $locator_btn; ?>
				</button>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div id="map"></div>
				<!-- 
				<?php // $jsonstore = \StoreLocator\Data::googleToJson('http://gsx2json.com/api?id=1LzIeBbnh0kQPb_E_lHvMtQGSj9nSzb-PMoefeD0tBYA&sheet=1&columns=false', 'AIzaSyB7w7AhjjiY6ul0aJ2_rXt0vZRpqiq9cX4'); ?>
				<script id="jsStore" data-api_key="AIzaSyB7w7AhjjiY6ul0aJ2_rXt0vZRpqiq9cX4" data-theme="<?php echo env('WP_THEME') ?>" src="./themes/<?php echo env('WP_THEME') ?>/assets/scripts/store.js"></script>
				<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7w7AhjjiY6ul0aJ2_rXt0vZRpqiq9cX4&libraries=places&callback=initMap"></script>
				 -->
			</div>
		</div>
	</div>
</section>