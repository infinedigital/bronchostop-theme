<?php
/*
* Template Name: Legal
*/
the_post();
$text_legal = get_field('text_legal');
$banner_tips = get_field( 'banner-tips-display' );

$modal = [];
$e     = 0;
?>
<section class="legal light-rose--bg">
	<?php
	if ( function_exists( 'yoast_breadcrumb' ) ) {
		yoast_breadcrumb( '<div id="breadcrumbs"><div class="content">', '</div></div>' );
	}
	?>
  <div class="container">
		<div class="max-width-lg white--bg centered">
			<div class="text-wrapper custom-wysiwyg">
				<h1 class="medium-title">
					<?php echo the_title(); ?>
				</h1>
		    <?php echo $text_legal;?>
			</div>
		</div>
  </div>
</section>
