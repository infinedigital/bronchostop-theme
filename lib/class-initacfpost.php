<?php
/**
* Adding ACF to Buy Now
* @package lactacyd
**/

// Remove Content editor on post
add_action('admin_init', 'remove_content');
function remove_content() {
	remove_post_type_support( 'post', 'editor' );
	remove_post_type_support( 'post', 'excerpt' );
}

class InitAcfPost{
	public function init()
	{
		acf_add_options_page();
		add_action('init', array($this, 'register_acf'));
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Post page',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'acf_after_title',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_textarea(
				[
					'name'          => 'excerpt',
					'label'         => 'Excerpt',
					'rows'          => 1,
					'instructions'  => 'A few words to describe the post. Displayed on all tips page and the home page. Maximum lenght : 200 characters.',
					'maxlength'     => 200,
				]
			),
			acf_flexible_content(
				[
					'name'         => 'content',
					'label'        => 'Content',
					'button_label' => 'Add a row',
					'layouts' => (
						[
							acf_layout(
								[
									'name' => 'one_column',
									'label' => 'One column',
									'layout' => 'table',
									'sub_fields' => [
										acf_wysiwyg(
											[
												'label'         => 'Text',
												'name'          => 'text',
												'required'      => true,
												'toolbar'       => 'basic',
												'media_upload'  => false,
											]
										),
									]
								]
							),
							acf_layout(
								[
									'name' => 'left_image',
									'label' => 'Left image',
									'layout' => 'table',
									'sub_fields' => [
										acf_image(
											[
												'name'          => 'image',
												'label'         => 'Image',
												'required'      => true,
												'return_format' => 'array',
												'wrapper'       => [ 'width' => 40 ],
											]
										),
										acf_wysiwyg(
											[
												'label'         => 'Text',
												'name'          => 'text',
												'toolbar'       => 'basic',
												'required'      => true,
												'media_upload'  => false,
												'wrapper'       => [ 'width' => 60 ],
											]
										),
									]
								]
							),
							acf_layout(
								[
									'name' => 'right_image',
									'label' => 'Right image',
									'layout' => 'table',
									'sub_fields' => [
										acf_wysiwyg(
											[
												'label'         => 'Text',
												'name'          => 'text',
												'toolbar'       => 'basic',
												'required'      => true,
												'media_upload'  => false,
												'wrapper'       => [ 'width' => 60 ],
											]
										),
										acf_image(
											[
												'name'          => 'image',
												'label'         => 'Image',
												'required'      => true,
												'return_format' => 'array',
												'wrapper'       => [ 'width' => 40 ],
											]
										),
									]
								]
							),
						]
					)
				]
			),
		];
		return $base_fields;
	}
	private function set_location() {
		$location = [
			[
				acf_location( 'post_type', '==', 'post' ),
			],
		];
		return $location;
	}
}
$acf_story = new InitAcfPost();
$acf_story->init();
