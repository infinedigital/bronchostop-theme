<?php
/**
* Adding ACF to Buy Now
* @package lactacyd
**/


class InitAcfBuyNow{
	public function init()
	{
		acf_add_options_page();
		add_action('init', array($this, 'register_acf'));
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Buy now page',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'acf_after_title',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_text(
				[
					'label'         => 'Page title',
					'name'          => 'title',
					'instructions'  => 'Title introducing retailers and store locator.',
					'default_value' => 'Select one relailer or find a store',
				]
			),
			acf_message(
				[
					'label'   => 'Add a new Store',
					'name'    => 'howto_message',
					'message' => 'Please edit the <a href="https://docs.google.com/spreadsheets/d/12ZXnDMoOBgWiuSZfEMDASEVifrCq5uCqZtKSRUqkH98/edit#gid=0" target="_blank" >Google Sheet</a>. You can use <a href="https://www.latlong.net/" target="_blank">this website</a> to find latitude and longitude.',
				]
			),
			acf_text(
				[
					'label'         => 'Search form label',
					'name'          => 'locator-label',
					'instructions'  => 'Translation of: Type your location and find a store near you.',
					'default_value' => 'Type your location and find a store near you',
					'wrapper'       => [ 'width' => 70 ],
				]
			),
			acf_text(
				[
					'name'          => 'locator-btn',
					'label'         => 'Search button',
					'required'      => true,
					'instructions'  => 'Text of the search button.',
					'default_value' => 'Search now',
					'wrapper'       => [ 'width' => 30 ],
				]
			),
		];
		return $base_fields;
	}
	private function set_location() {
		$location = [
			[
				acf_location( 'page_template', '==', 'page-buynow.php' ),
			],
		];
		return $location;
	}
}
$acf_story = new InitAcfBuyNow();
$acf_story->init();
