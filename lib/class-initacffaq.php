<?php
/**
* Adding ACF to Buy Now
* @package lactacyd
**/


class InitAcfFaq{
	public function init()
	{
		acf_add_options_page();
		add_action('init', array($this, 'register_acf'));
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'FAQ page',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'acf_after_title',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_tab(
				[
					'label' => 'FAQ',
					'name'  => 'faq_tab',
				]
			),
			acf_repeater(
				[
					'label'        => 'FAQ',
					'name'         => 'faq',
					'layout'       => 'block',
					'sub_fields'   => [
						acf_textarea(
							[
								'label' => 'Title',
								'name'  => 'title',
								'rows'  => '1',
							]
						),
						acf_wysiwyg(
							[
								'label'        => 'Response',
								'name'         => 'content',
								'toolbar'      => 'basic',
								'media_upload' => false
							]
						),
					]
				]
			),
			acf_tab(
				[
					'label' => 'Related products',
					'name'  => 'related_tab',
				]
			),
			acf_group(
				[
					'label'        => '',
					'name'         => 'related',
					'sub_fields'   => [
						acf_textarea(
							[
								'label'         => 'Title',
								'name'          => 'title',
								'rows'          => '1',
								'instructions'  => 'Title of related products.',
								'placeholder'   => 'Your solution to relieve any cough',
								'default_value' => 'Your solution to relieve any cough',
								'required'      => true,
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_link(
							[
								'label'         => 'Link',
								'name'          => 'link',
								'instructions'  => 'Link to all products.' ,
								'required'      => true,
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_relationship(
							[
								'label'         => 'Select products to display',
								'name'          => 'selection',
								'post_type'     => [ 'product' ],
								'filters'       => false,
								'elements'      => [ 'featured_image' ],
								'required'      => false,
								'instructions'  => 'Select a product to display as a related product on the bottom of the page. Leave it empty to hide the section.',
								'return_format' => 'object',
							]
						),
					]
				]
			),
		];
		return $base_fields;
	}
	private function set_location() {
		$location = [
			[
				acf_location( 'page_template', '==', 'page-faq.php' ),
			],
		];
		return $location;
	}
}
$acf_story = new InitAcfFaq();
$acf_story->init();
