<?php
/**
* Adding ACF to Buy Now
* @package lactacyd
**/


class InitAcfHome{
	public function init()
	{
		acf_add_options_page();
		add_action('init', array($this, 'register_acf'));
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Home page',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'acf_after_title',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_tab(
				[
					'name'  => 'landing_tab',
					'label' => 'Landing Hero',
				]
			),
			acf_group(
				[
					'name'         => 'landing',
					'label'        => '',
					'sub_fields' => [
						acf_group(
							[
								'name'       => 'content',
								'label'      => '',
								'wrapper'    => [ 'width' => 65 ],
								'sub_fields' => [
									acf_text(
										[
											'name'          => 'text',
											'label'         => 'Text',
											'instructions'  => 'Text under the baseline.',
											'placeholder'   => 'Bronchostop® is your ally to relieve cough. Any cough.',
											'default_value' => 'Bronchostop® is your ally to relieve cough. Any cough.',
										]
									),
									acf_link(
										[
											'name'         => 'btn',
											'label'        => 'Button',
											'instructions' => 'Discover the range button',
										]
									),
								]
							]
						),
						acf_image(
							[
								'name'          => 'image',
								'label'         => 'Key Visual',
								'instructions'  => 'Right Key visual.',
								'return_format' => 'array',
								'wrapper'       => [ 'width' => 35 ],
							]
						),
						acf_group(
							[
								'name'         => 'video',
								'label'        => 'Landing video',
								'sub_fields' => [
									acf_true_false(
										[
											'name'          => 'display',
											'label'         => 'Display',
											'instructions'  => 'Do you need video ad call to action?',
											'wrapper'       => [ 'width' => 50 ],
											'ui'            => 1,
											'ui_on_text'    => 'Yes please',
											'ui_off_text'   => 'No, thanks',
										]
									),
									acf_true_false(
										[
											'name'          => 'embended',
											'label'         => 'Embended video',
											'instructions'  => 'Is this a embended video or a YouTube video?',
											'wrapper'       => [ 'width' => 50 ],
											'ui'            => 1,
											'ui_on_text'    => 'Embended video',
											'ui_off_text'   => 'Youtube video',
										]
									),
									acf_group(
										[
											'name'         => 'content',
											'label'        => '',
											'wrapper'      => [ 'width' => 50 ],
											'conditional_logic' => [ [ acf_conditional( 'display', 1 ) ] ],
											'sub_fields' => [
												acf_textarea(
													[
														'name'          => 'title',
														'label'         => 'Video title',
														'instructions'  => '"Video" text title',
														'rows'          => 1,
														'placeholder'   => 'Video',
														'default_value' => 'Video',
													]
												),
												acf_textarea(
													[
														'name'          => 'text',
														'label'         => 'Video text',
														'instructions'  => '"Video" text for the call to action',
														'rows'          => 1,
														'placeholder'   => 'Watch the ad',
														'default_value' => 'Watch the ad',
													]
												),
											]
										]
									),
									acf_image(
										[
											'name'          => 'image',
											'label'         => 'Ad image',
											'return_format' => 'array',
											'wrapper'       => [ 'width' => 50 ],
											'conditional_logic' => [ [ acf_conditional( 'display', 1 ) ] ],
										]
									),
									acf_url(
										[
											'name'         => 'link',
											'label'        => 'Link of the video',
											'conditional_logic' => [ [ acf_conditional( 'display', 1 ) ] ],
										]
									),
								]
							]
						),
					]
				]
			),
			acf_tab(
				[
					'name'  => 'products_tab',
					'label' => 'Products call to action',
				]
			),
			acf_group(
				[
					'name'         => 'products',
					'label'        => '',
					'sub_fields' => [
						acf_select(
							[
								'name'          => 'display',
								'label'         => 'Section display type',
								'instructions'  => 'Choose the type of display you want.',
								'choices' => array(
									'two' => 'Two product'
								),
								'default_value' => array( 0 => 'one' ),
							]
						),
						acf_textarea(
							[
								'name'          => 'title',
								'label'         => 'Title',
								'instructions'  => 'Title of the section',
								'placeholder'   => 'Any cough relief, dicover Bronchostop® range',
								'default_value' => 'Any cough relief, dicover Bronchostop® range',
								'rows'          => 2,
							]
						),
						acf_group(
							[
								'name'       => 'left',
								'label'      => 'Left',
								'wrapper'    => [ 'width' => 50 ],
								'sub_fields' => [
									acf_wysiwyg(
										[
											'name'          => 'title',
											'label'         => 'Title',
											'instructions'  => 'Title of the left block',
											'placeholder'   => 'The <strong>adult</strong> range to relieve any cough',
											'default_value' => 'The <strong>adult</strong> range to relieve any cough',
											'media_upload'  => false,
											'toolbar'       => 'basic',
										]
									),
									acf_link(
										[
											'name'         => 'link',
											'label'        => 'Button',
											'instructions' => 'Left block button',
										]
									),
									acf_image(
										[
											'name'          => 'image',
											'label'         => 'Left block image',
											'return_format' => 'array',
										]
									),
								]
							]
						),
						acf_group(
							[
								'name'       => 'right',
								'label'      => 'Right',
								'wrapper'    => [ 'width' => 50 ],
								'sub_fields' => [
									acf_wysiwyg(
										[
											'name'          => 'title',
											'label'         => 'Title',
											'instructions'  => 'Title of the right block',
											'placeholder'   => 'Our solution to relieve <strong>children</strong> cough',
											'default_value' => 'Our solution to relieve <strong>children</strong> cough',
											'media_upload'  => false,
											'toolbar'       => 'basic',
										]
									),
									acf_link(
										[
											'name'         => 'link',
											'label'        => 'Button',
											'instructions' => 'Right block button',
										]
									),
									acf_image(
										[
											'name'          => 'image',
											'label'         => 'Right block image',
											'return_format' => 'array',
										]
									),
								]
							]
						),
					]
				]
			),
			acf_tab(
				[
					'name'  => 'why_tab',
					'label' => 'Why section',
				]
			),
			acf_group(
				[
					'name'       => 'why',
					'label'      => '',
					'sub_fields' => [
						acf_group(
							[
								'label'        => '',
								'name'         => 'images',
								'wrapper'       => [ 'width' => 30 ],
								'sub_fields'   => [
									acf_image(
										[
											'label'         => 'Large image',
											'name'          => 'image-1',
											'return_format' => 'array',
											'required'      => true,
										]
									),
									acf_image(
										[
											'label'         => 'Small image',
											'name'          => 'image-2',
											'return_format' => 'array',
											'required'      => true,
										]
									),
								]
							]
						),
						acf_group(
							[
								'label'        => '',
								'name'         => 'content',
								'wrapper'      => [ 'width' => 70 ],
								'sub_fields'   => [
									acf_text(
										[
											'label'   => 'Title',
											'name'    => 'title',
											'default_value' => 'Why Bronchostop®?',
										]
									),
									acf_wysiwyg(
										[
											'label'         => 'Text',
											'name'          => 'text',
											'toolbar'       => 'basic',
											'media_upload'  => false,
											'default_value' => '<p>Thanks to the action of both its ingredients, Bronchostop is suitable for any cough and it\'s available in different formats. Suitable for adults and children over 12 year.</p><p>Bronchostop relieves dry + chesty coughs. No need to worry about what type of cough you are experiencing.</p>',
										]
									),
									acf_link(
										[
											'label'         => 'Button',
											'name'          => 'btn',
										]
									),
								]
							]
						)
					]
				]
			),
			acf_tab(
				[
					'name'  => 'tips_tab',
					'label' => 'Tips'
				]
			),
			acf_group(
				[
					'label'        => '',
					'name'         => 'tips',
					'sub_fields'   => [
						acf_link(
							[
								'name'     => 'link',
								'label'    => 'Tips link',
							]
						),
						acf_relationship(
							[
								'label'         => 'Tips',
								'name'          => 'selected',
								'post_type'     => [ 'post' ],
								'filters'       => false,
								'elements'      => [ 'featured_image' ],
								'required'      => false,
								'instructions'  => 'Select tips that you want at the bottom of the homepage. Let it empty to hide this section',
								'return_format' => 'object',
							]
						),
					]
				]
			),
		];
		return $base_fields;
	}
	private function set_location() {
		$location = [
			[
				acf_location( 'page_type', '==', 'front_page' ),
			],
		];
		return $location;
	}
}
$acf_story = new InitAcfHome();
$acf_story->init();
