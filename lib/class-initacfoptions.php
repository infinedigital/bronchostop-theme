<?php
/**
* Adding ACF to Page Options
* @package infine
**/

class InitAcfOptions {
	public function init() {
		acf_add_options_page();
		add_action( 'init', array( $this, 'register_acf' ) );
	}
	
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Options',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'normal',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
		acf_field_group(
			[
				'title'           => 'Facts',
				'fields'          => $this->register_facts_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'normal',
				'label_placement' => 'top',
				'menu_order'      => 1,
			]
		);
		acf_field_group(
			[
				'title'           => 'Other options',
				'fields'          => $this->register_reusable_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'normal',
				'label_placement' => 'top',
				'menu_order'      => 2,
			]
		);
	}
	
	private function register_base_fields() {
		$base_fields = [
			acf_tab(
				[
					'name'  => 'menu',
					'label' => 'Menu navigation',
				]
			),
			acf_repeater(
				[
					'name'         => 'menu-pages',
					'label'        => 'Pages links',
					'instructions' => 'Usually links to the pages, displayed in blue. Drag and drop to re-order.',
					'wrapper'      => [ 'width' => 50 ],
					'sub_fields'   => [
						acf_link(
							[
								'name'         => 'link',
								'label'        => 'Link to page',
								'instructions' => 'use the "Title" attribute for the label displayed in the button',
							]
						),
					],
				]
			),
			acf_link(
				[
					'name'  => 'menu-buynow',
					'label' => 'Buy now link',
					'instructions' => 'Link to the buy now page. The translation of the button is in "multiple used words".',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_true_false(
				[
					'name'         => 'lang',
					'label'        => 'Multiple Languages',
					'instructions' => 'Do you need a lang switcher',
					'wrapper'      => [ 'width' => 50 ],
					'ui'            => 1,
					'ui_on_text'    => 'Yes please',
					'ui_off_text'   => 'No, thanks',
				]
			),
			acf_tab(
				[
					'name'  => 'brand_tab',
					'label' => 'About the brand',
				]
			),
			acf_group(
				[
					'name'         => 'brand',
					'label'        => 'Brand informations',
					'instructions' => 'All brands text used in the site. Please contact in Fine if you need more words.',
					'sub_fields' => [
						acf_image(
							[
								'name'          => 'logo',
								'label'         => 'Brand Logo',
								'instructions'  => 'Logo used in every area of the website and footer.',
								'return_format' => 'array',
							]
						),
						acf_text(
							[
								'name'         => 'name',
								'label'        => 'Brand name',
								'instructions' => 'Displayed in alt attribute and other SEO elements',
								'placeholder'   => 'Bronchostop',
								'default_value' => 'Bronchostop',
								'wrapper'      => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'slogan',
								'label'         => 'Brand slogan',
								'instructions'  => 'Displayed in alt attribute and other SEO elements',
								'placeholder'   => 'Relieves ANY cough',
								'default_value' => 'Relieves ANY cough',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'         => 'phone',
								'label'        => 'Phone',
								'wrapper'      => [ 'width' => 50 ],
								'placeholder'   => '+32 (0)9 381 02 00',
								'default_value' => '+32 (0)9 381 02 00',
								'instructions' => 'Customer service phone used for contact page and footer.',
							]
						),
						acf_textarea(
							[
								'name' => 'address',
								'label' => 'Address of the office',
								'rows' => '2',
								'placeholder' => 'Industrial Zoning De Prijkels - Venecoweg 26 9810 Nazareth - Belgium',
								'default_value' => 'Industrial Zoning De Prijkels - Venecoweg 26 9810 Nazareth - Belgium',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'    => 'fax',
								'label'   => 'Fax',
								'wrapper' => [ 'width' => 50 ],
								'placeholder'   => '+32 (0)9 381 02 20',
								'default_value' => '+32 (0)9 381 02 20',
								'instructions'  => 'Customer service fax used for contact page and footer.',
							]
						),
						acf_text(
							[
								'name'    => 'mail',
								'label'   => 'Mail',
								'wrapper' => [ 'width' => 50 ],
								'placeholder'   => 'info@omega-pharma.com',
								'default_value' => 'info@omega-pharma.com',
								'instructions'  => 'Customer service mail used for contact page and footer.',
							]
						),
					],
				]
			),
			acf_tab(
				[
					'name'  => 'generic_tab',
					'label' => 'Multiple used words',
				]
			),
			acf_group(
				[
					'name'         => 'generic',
					'label'        => 'Product page words',
					'instructions' => 'Displayed in product page. Please contact in Fine if you need more words.',
					'sub_fields'   => [
						acf_text(
							[
								'name'          => 'pack_size',
								'label'         => 'Pack size label',
								'instructions'   => 'Translation of: Pack size in product page.',
								'default_value' => 'Pack size',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'buy_now',
								'label'         => 'Buy now label',
								'default_value' => 'Buy now',
								'instructions'   => 'Translation of: Buy now',
								'placeholder'   => 'Buy now',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'more_info',
								'label'         => 'More info label',
								'default_value' => 'More info',
								'instructions'  => 'Translation of: More info',
								'placeholder'   => 'More info',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'all_products',
								'label'         => 'All products label',
								'default_value' => 'All products',
								'instructions'  => 'Translation of: All products',
								'placeholder'   => 'All products',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'no_result',
								'label'         => 'No result found',
								'default_value' => 'Sorry, no result found',
								'placeholder'   => 'Sorry, no result found',
								'instructions'  => 'Displayed in every page that features a searchform and the 404.',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'customer_service',
								'label'         => 'Customer service',
								'default_value' => 'Customer service',
								'placeholder'   => 'Customer service',
								'instructions'  => 'Displayed in footer and contact page.',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'tips_cough',
								'label'         => 'Some tips about cough',
								'default_value' => 'Some tips about cough',
								'placeholder'   => 'Some tips about cough',
								'instructions'  => 'Translation of: Some tips about cough',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'all_tips',
								'label'         => 'All tips',
								'default_value' => 'All tips',
								'placeholder'   => 'All tips',
								'instructions'  => 'Translation of: All tips',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'tips_next',
								'label'         => 'Next tip',
								'default_value' => 'Next tip',
								'placeholder'   => 'Next tip',
								'instructions'  => 'Translation of: Next tip',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'read_more',
								'label'         => 'Read more',
								'default_value' => 'Read more',
								'placeholder'   => 'Read more',
								'instructions'  => 'Displayed in tips page.',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
					],
				]
			),
			acf_tab(
				[
					'name'  => 'footer_tab',
					'label' => 'Footer general',
				]
			),
			acf_group(
				[
					'name' => 'footer',
					'label' => 'Footer',
					'instructions' => 'Copyright, logo and generic legal mentions',
					'sub_fields' => [
						acf_image(
							[
								'name'          => 'perrigo',
								'label'         => 'Perrigo logo',
								'instructions'  => 'Please use svg format',
								'return_format' => 'array',
								'required'      => true,
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'copy',
								'label'         => 'Copyright generic text',
								'required'      => true,
								'default_value' => 'Copywright 2020 PERRIGO - All rights reserved',
								'placeholder'   => 'Copywright 2020 PERRIGO - All rights reserved',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_wysiwyg(
							[
								'name'         => 'footnote',
								'label'        => 'Generic footnotes',
								'toolbar'      => 'basic',
								'media_upload' => false,
								'instructions' => 'Mentions visible in every page. Use only list and text elements.',
							]
						),
					],
				]
			),
			acf_group(
				[
					'name' => 'legal',
					'label' => 'Cookies and terms',
					'required' => true,
					'instructions' => 'Cookies,terms and conditions labels in the footer',
					'sub_fields' => [
						acf_link(
							[
								'name' => 'cookies',
								'label' => 'Cookies policy',
								'instructions' => 'Link to : Privacy policy | Cookies preferences',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
						acf_link(
							[
								'name' => 'privacy',
								'label' => 'Privacy statement',
								'instructions' => 'Link to : Privacy statement',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
						acf_link(
							[
								'name' => 'terms',
								'label' => 'Terms & Conditions',
								'instructions' => 'Link to : Terms & Conditions',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
					],
				]
			),
			acf_repeater(
				[
					'name'  => 'social-media',
					'label' => 'Social media links',
					'instructions' => 'The social media links for your brand displayed in the footer',
					'sub_fields' => [
						acf_select(
							[
								'label' => 'Social media item',
								'name' => 'social__name',
								'instructions' => 'Select the media related',
								'wrapper' => array(
									'width' => '50',
								),
								'choices' => array(
									'facebook' => 'Facebook',
									'instagram' => 'Instagram',
									'twitter' => 'Twitter',
									'linkedin' => 'LinkedIn',
									'youtube' => 'YouTube',
									'---' => '---',
									'behance' => 'Behance',
									'discord' => 'Discord',
									'dribbble' => 'Dribbble',
									'flickr' => 'Flickr',
									'medium' => 'Medium',
									'pinterest' => 'Pinterest',
									'skype' => 'Skype',
									'tiktok' => 'TikTok',
									'tumblr' => 'Tumblr',
									'twitch' => 'Twitch',
								),
								'default_value' => array(
									0 => 'facebook',
								),
							]
						),
						acf_link(
							[
								'label' => 'Link',
								'name' => 'social__url',
								'instructions' => 'The URL of your page',
								'wrapper' => array(
									'width' => '50',
								),
							]
						),
					],
				]
			),
			acf_tab(
				[
					'name'  => 'retailers_tab',
					'label' => 'Retailers',
				]
			),
			acf_group(
				[
					'name'         => 'cta',
					'label'        => 'Call to action',
					'instructions' => 'Footer call to action for retailers.',
					'sub_fields'   => [
						acf_textarea(
							[
								'name'          => 'title',
								'label'         => 'Titre',
								'default_value' => 'Available in many stores',
								'placeholder'   => 'Available in many stores',
								'rows'          => 2,
								'instructions'  => 'Title of the CTA.',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_textarea(
							[
								'name'          => 'content',
								'label'         => 'Content',
								'default_value' => 'Have a look at all stores where you can find our product.',
								'placeholder'   => 'Have a look at all stores where you can find our product.',
								'rows'          => 2,
								'instructions'  => 'Text of the CTA.',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_link(
							[
								'name'          => 'link',
								'label'         => 'Link',
								'instructions'  => 'Link of the CTA. Should go to the store locatore page.',
							]
						),
					]
				]
			),
			acf_repeater(
				[
					'name'         => 'retailers',
					'label'        => 'Retailers',
					'layout'       => 'block',
					'instructions' => 'Insert the logo, link and google ID tracking if needed.',
					'sub_fields' => [
						acf_image(
							[
								'name'          => 'logo',
								'label'         => 'Logo',
								'instructions'  => 'Please use format 148x80px',
								'return_format' => 'array',
								'required'      => true,
								'wrapper'       => [
									'width' => 33,
								],
							]
						),
						acf_link(
							[
								'name'     => 'link',
								'label'    => 'Retailer link',
								'required' => true,
								'wrapper'  => [
									'width' => 33,
								],
							]
						),
						acf_text(
							[
								'name'    => 'id',
								'label'   => 'Google tag ID',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
					],
				]
			),
			acf_tab(
				[
					'name'  => '404_tab',
					'label' => '404 page',
				]
			),
			acf_text(
				[
					'name'          => 'title_404',
					'label'         => '404 page title',
					'required'      => true,
					'default_value' => 'The page you are looking for can not be found.',
					'instructions'  => 'Translation of: The page you are looking for can not be found.',
				]
			),
			acf_text(
				[
					'name'          => 'subtitle_404',
					'label'         => '404 question',
					'required'      => true,
					'default_value' => 'While we have you, did you know that',
					'instructions'  => 'Type a few words about this brand.',
				]
			),
			acf_textarea(
				[
					'name'          => 'desc_404',
					'label'         => 'Fun fact',
					'rows'          => '3',
					'instructions'  => 'Second part of your introduction if wanted',
				]
			),
			acf_link(
				[
					'name'          => 'first_link_404',
					'label'         => 'Link to main page',
					'instructions'  => 'Link to the main page you would like to send.',
					'wrapper'       => [ 'width' => 50 ],
				]
			),
			acf_link(
				[
					'name'          => 'second_link_404',
					'label'         => 'Link to second page',
					'instructions'  => 'Usually link to the homepage',
					'wrapper'       => [ 'width' => 50 ],
				]
			),
		];
		return $base_fields;
	}
	
	private function register_reusable_fields() {
		$base_fields = [
			acf_textarea(
				[
					'name' => 'google_tag',
					'label' => 'Google tag code',
					'rows' => '5',
					'instructions' => 'Insert complete Google Tag Manager code. Please do not add the noscript tag. Comments can be removed. This code goes as high in the <head> of the page as possible.',
					'default_value' => '<script></script>'
				]
			),
			acf_textarea(
				[
					'name' => 'google_tag_noscript',
					'label' => 'No script code for Google Tag',
					'rows' => '2',
					'instructions' => 'This code goes immediately after the opening <body> tag.',
					'default_value' => '<noscript></noscript>'
				]
			),
		];
		return $base_fields;
	}
	
	private function register_facts_fields() {
		$base_fields = [
			acf_group(
				[
					'name'         => 'module_facts',
					'label'        => 'Facts module',
					'instructions' => 'Configuration for the facts slider.',
					'sub_fields' => [
						acf_textarea(
							[
								'name'          => 'title',
								'label'         => 'Title',
								'rows'          => '1',
								'wrapper'       => [ 'width' => 50 ],
								'default_value' => 'Some facts about cough',
								'instructions'  => 'Right left small title.',
							]
						),
						acf_link(
							[
								'name'          => 'btn',
								'label'         => 'Button',
								'instructions'  => 'Use title as the label of the button and choose your link target',
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_text(
							[
								'name'          => 'prev',
								'label'         => 'Previous fact text',
								'wrapper'       => [ 'width' => 50 ],
								'default_value' => 'Prev fact',
								'instructions'  => 'Text for the previous fact button.',
							]
						),
						acf_text(
							[
								'name'          => 'next',
								'label'         => 'Next fact text',
								'wrapper'       => [ 'width' => 50 ],
								'default_value' => 'Next fact',
								'instructions'  => 'Text for the next fact button.',
							]
						),
						acf_repeater(
							[
								'name'         => 'facts',
								'label'        => 'Facts',
								'layout'       => 'table',
								'sub_fields' => [
									acf_textarea(
										[
											'name'          => 'title',
											'label'         => 'Title',
											'rows'          => '5',
											'new_lines'     => 'br',
											'wrapper'       => [ 'width' => 50 ],
											'instructions'  => 'Title/big text of the fact',
										]
									),
									acf_textarea(
										[
											'name'          => 'content',
											'label'         => 'Content',
											'rows'          => '5',
											'new_lines'     => 'wpautop',
											'wrapper'       => [ 'width' => 50 ],
											'instructions'  => 'Content/small text of the fact',
										]
									),
								],
							]
						),
					]
				]
			),
		];
		return $base_fields;
	}
	
	private function set_location() {
		$location = [
			[
				acf_location( 'options_page', '==', 'acf-options' ),
			],
		];
		return $location;
	}
}

$acf_home = new InitAcfOptions();
$acf_home->init();