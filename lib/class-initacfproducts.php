<?php
/**
* Adding ACF to Page Home
* @package infine
**/

class InitAcfProducts {
	public function init() {
		acf_add_options_page();
		add_action( 'init', array( $this, 'register_acf' ) );
	}
	
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Products',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'acf_after_title',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
	}
	
	private function register_base_fields() {
		$base_fields = [
			acf_relationship(
				[
					'label'         => 'Select products to display',
					'name'          => 'selection',
					'post_type'     => [ 'product' ],
					'filters'       => false,
					'elements'      => [ 'featured_image' ],
					'required'      => false,
					'instructions'  => 'Select a product to display as a related product on the bottom of the page.',
					'return_format' => 'object',
				]
			),
		];
		return $base_fields;
	}
	
	private function set_location() {
		$location = [
			[
				acf_location( 'page_template', '==', 'page-products.php' ),
			],
		];
		return $location;
	}
}

$acf_product = new InitAcfProducts();
$acf_product->init();