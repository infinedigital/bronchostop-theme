<?php
/**
* Adding ACF to Page Common
* @package infine
**/

class InitAcfCommon {
	public function init()
	{
		acf_add_options_page();
		add_action('init', array($this, 'register_acf'));
		add_action(
			'init', function () {
				remove_post_type_support('page', 'editor');
				remove_post_type_support('page', 'comments');
				remove_post_type_support('page', 'author');
				remove_post_type_support('page', 'revisions');
				remove_post_type_support('page', 'slug');
				remove_post_type_support('page', 'thumbnail');
			}
		);
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Page options',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'normal',
				'label_placement' => 'bottom',
				'menu_order'      => 99,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_wysiwyg(
				[
					'name'         => 'reference',
					'label'        => 'Reference',
					'media_upload' => false,
					'toolbar'      => 'basic',
				]
			),
			acf_group(
				[
					'label'        => 'Footer options',
					'name'         => 'footer_display',
					'sub_fields'   => [
						acf_true_false(
							[
								'label'         => 'CTA store locator',
								'name'          => 'display_cta',
								'instructions'  => 'Do you want to display "Available in many stores" call to action for this page? Select "Yes" if you do.',
								'default_value' => 1,
								'wrapper'       => [ 'width' => 50 ],
								'ui'            => 1,
								'ui_on_text'    => 'Yes please',
								'ui_off_text'   => 'No, thanks',
							]
						),
						acf_true_false(
							[
								'label'         => 'Retailers',
								'name'          => 'display_retailers',
								'instructions'  => 'Do you want to display retailers slider? Select "Yes" if you do. Retailers can be changed in the option page.',
								'default_value' => 1,
								'wrapper'       => [ 'width' => 50 ],
								'ui'            => 1,
								'ui_on_text'    => 'Yes please',
								'ui_off_text'   => 'No, thanks',
							]
						),
					]
				]
			)
		];
		return $base_fields;
	}
	private function set_location() {
		$location = [
			[
				acf_location( 'post_type', 'page' ),
			],
			[
				acf_location( 'post_type', 'product' ),
			],
			[
				acf_location( 'post_type', 'post' ),
			],
		];
		return $location;
	}
}
$acf_common = new InitAcfCommon();
$acf_common->init();
