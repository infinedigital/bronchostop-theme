<?php
/**
 * Adding ACF to Legal
 * @package infine
 **/


class InitAcfLegal{
    public function init()
    {
        acf_add_options_page();
        add_action('init', array($this, 'register_acf'));
        add_action(
            'init', function () {
            remove_post_type_support('page', 'editor');
            remove_post_type_support('page', 'comments');
            remove_post_type_support('page', 'author');
            remove_post_type_support('page', 'revisions');
            remove_post_type_support('page', 'slug');
            remove_post_type_support('page', 'thumbnail');
        }
        );
    }
    public function register_acf() {
        acf_field_group(
            [
                'title'           => 'Legal',
                'fields'          => $this->register_base_fields(),
                'style'           => 'default',
                'location'        => $this->set_location(),
                'position'        => 'acf_after_title',
                'label_placement' => 'top',
                'menu_order'      => 0,
            ]
        );
    }
    private function register_base_fields() {
        $base_fields = [
            acf_wysiwyg(
                [
                    'name'         => 'text_legal',
                    'label'        => 'Text content Legal',
                    'instructions' => 'Set text in bold or create list. Do not insert titles in  this section.',
                    'toolbar'      => 'basic',
                    'media_upload' => false,
                    'wrapper'    => [
                        'width' => 100,
                    ],
                ]
            ),
        ];
        return $base_fields;
    }
    private function set_location() {
        $location = [
            [
                acf_location( 'page_template', '==', 'page-legal.php' ),
            ],
        ];
        return $location;
    }
}
$acf_story = new InitAcfLegal();
$acf_story->init();
