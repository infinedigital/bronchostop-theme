<?php
/**
* Adding ACF to Buy Now
* @package lactacyd
**/


class InitAcfWhy{
	public function init()
	{
		acf_add_options_page();
		add_action('init', array($this, 'register_acf'));
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Why Bronchostop page',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'acf_after_title',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_tab(
				[
					'label' => 'First section',
					'name'  => 'intro_tab',
				]
			),
			acf_group(
				[
					'label'        => 'Number 1',
					'name'         => 'first',
					'sub_fields'   => [
						acf_image(
							[
								'label'         => 'Image',
								'name'          => 'image',
								'return_format' => 'array',
								'required'      => true,
								'wrapper'       => [ 'width' => 30 ],
							]
						),
						acf_group(
							[
								'label'        => '',
								'name'         => 'content',
								'wrapper'       => [ 'width' => 70 ],
								'sub_fields'   => [
									acf_text(
										[
											'label'   => 'Title',
											'name'    => 'title',
											'default_value' => 'Bronchostop® relieves ANY type of cough',
										]
									),
									acf_wysiwyg(
										[
											'label'         => 'Text',
											'name'          => 'text',
											'toolbar'       => 'basic',
											'media_upload'  => false,
											'default_value' => '<p><strong>Do you know that 72% of people don’t know what type of cough they have?*</strong></p><p>72% of people are not aware of different types of cough, from dry to chesty. Bronchostop do not require you to know. Bronchostop relieves cough, whatever the type.<p><p><small>* Consumer research, April 2015</small><p>',
										]
									),
								]
							]
						),
					]
				]
			),
			acf_tab(
				[
					'label' => 'Second section',
					'name'  => 'second_tab',
				]
			),
			acf_group(
				[
					'label'        => 'Number 2',
					'name'         => 'second',
					'sub_fields'   => [
						acf_group(
							[
								'label'        => '',
								'name'         => 'content',
								'wrapper'       => [ 'width' => 70 ],
								'sub_fields'   => [
									acf_text(
										[
											'label'   => 'Title',
											'name'    => 'title',
											'default_value' => 'Bronchostop® has a good safety profile',
										]
									),
									acf_wysiwyg(
										[
											'label'         => 'Text',
											'name'          => 'text',
											'toolbar'       => 'basic',
											'media_upload'  => false,
											'default_value' => '<p>Non-drowsy. Gluten-free. Alcohol-free. Lactose-free. No genetically modified organisms.<p>',
										]
									),
								]
							]
						),
						acf_repeater(
							[
								'label'        => 'Icons',
								'name'         => 'icon-list',
								'layout'       => 'table',
								'instructions' => 'Add icons of singularity. Please prefer square format and svg icon.',
								'wrapper'      => [ 'width' => 30 ],
								'sub_fields'   => [
									acf_image(
										[
											'label' => 'Icon',
											'name'  => 'icon',
											'return_format' => 'array',
										]
									),
								],
							]
						),
					]
				]
			),
			acf_tab(
				[
					'label' => 'Third section',
					'name'  => 'third_tab',
				]
			),
			acf_group(
				[
					'label'        => 'Number 3',
					'name'         => 'third',
					'sub_fields'   => [
						acf_image(
							[
								'label'         => 'Image',
								'name'          => 'image',
								'return_format' => 'array',
								'required'      => true,
								'wrapper'       => [ 'width' => 30 ],
							]
						),
						acf_group(
							[
								'label'        => '',
								'name'         => 'content',
								'wrapper'       => [ 'width' => 70 ],
								'sub_fields'   => [
									acf_text(
										[
											'label'   => 'Title',
											'name'    => 'title',
											'default_value' => 'Bronchostop® exclusive formula',
										]
									),
									acf_wysiwyg(
										[
											'label'         => 'Text',
											'name'          => 'text',
											'toolbar'       => 'basic',
											'media_upload'  => false,
											'default_value' => '<p>With thyme and marshmallow extract.<p>',
										]
									),
								]
							]
						),
					]
				]
			),
			acf_tab(
				[
					'label' => 'Fourth section',
					'name'  => 'fourth_tab',
				]
			),
			acf_group(
				[
					'label'        => 'Number 4',
					'name'         => 'forth',
					'sub_fields'   => [
						acf_group(
							[
								'label'        => '',
								'name'         => 'content',
								'wrapper'       => [ 'width' => 70 ],
								'sub_fields'   => [
									acf_text(
										[
											'label'   => 'Title',
											'name'    => 'title',
											'default_value' => 'Bronchostop® for the whole family',
										]
									),
									acf_wysiwyg(
										[
											'label'         => 'Text',
											'name'          => 'text',
											'toolbar'       => 'basic',
											'media_upload'  => false,
											'default_value' => '<p>Bronchostop® range offers solutions suitable to the whole family.</p>',
										]
									),
									acf_link(
										[
											'label'         => 'Button',
											'name'          => 'btn',
										]
									),
								]
							]
						),
						acf_group(
							[
								'label'        => '',
								'name'         => 'images',
								'wrapper'       => [ 'width' => 30 ],
								'sub_fields'   => [
									acf_image(
										[
											'label'         => 'Large image',
											'name'          => 'image-1',
											'return_format' => 'array',
											'required'      => true,
										]
									),
									acf_image(
										[
											'label'         => 'Small image',
											'name'          => 'image-2',
											'return_format' => 'array',
											'required'      => true,
										]
									),
								]
							]
						),
					]
				]
			),
			acf_tab(
				[
					'label' => 'Fifth section',
					'name'  => 'fifth_tab',
				]
			),
			acf_group(
				[
					'label'        => 'Number 5',
					'name'         => 'fifth',
					'sub_fields'   => [
						acf_image(
							[
								'label'         => 'Image',
								'name'          => 'image',
								'return_format' => 'array',
								'required'      => true,
								'wrapper'       => [ 'width' => 30 ],
							]
						),
						acf_group(
							[
								'label'        => '',
								'name'         => 'content',
								'wrapper'       => [ 'width' => 70 ],
								'sub_fields'   => [
									acf_text(
										[
											'label'   => 'Title',
											'name'    => 'title',
											'default_value' => 'Bronchostop® is available in different formats',
										]
									),
									acf_wysiwyg(
										[
											'label'         => 'Text',
											'name'          => 'text',
											'toolbar'       => 'basic',
											'media_upload'  => false,
											'default_value' => '<p>Bronchostop range includes multiple solutions to relieve ANY cough. If you prefer syrup, Bronchostop is <a href="">suitable in different fromats</a>: 120 or 200 ml.</p><p>Looking for on-the-go solution? <a href="">Try Bronchostop Pastilles</a>.</p>',
										]
									),
								]
							]
						),
					]
				]
			),
		];
		return $base_fields;
	}
	private function set_location() {
		$location = [
			[
				acf_location( 'page_template', '==', 'page-why.php' ),
			],
		];
		return $location;
	}
}
$acf_story = new InitAcfWhy();
$acf_story->init();
