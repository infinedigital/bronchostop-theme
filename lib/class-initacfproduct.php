<?php
/**
* Adding ACF to Page Home
* @package infine
**/

class InitAcfProduct {
	public function init() {
		acf_add_options_page();
		add_action( 'init', array( $this, 'register_acf' ) );
	}
	
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Product',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'acf_after_title',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
	}
	
	private function register_base_fields() {
		$base_fields = [
			acf_tab(
				[
					'label' => 'Product',
					'name'  => 'product_tabs',
				]
			),
			acf_true_false(
				[
					'label'         => '"New" label display',
					'name'          => 'new',
					'instructions'  => 'Is it a "New" product? Select "Yes" if you do. Go to the options to upload your label image.',
					'wrapper'       => [ 'width' => 50 ],
					'ui'            => 1,
					'ui_on_text'    => 'Yes please',
					'ui_off_text'   => 'No, thanks',
				]
			),
			acf_image(
				[
					'label'         => 'Pack image',
					'name'          => 'pack',
					'instructions'  => 'Here is the pack image. Do not forget the "Thumbnail" image on the right aside, for the product listing. Please Contact In Fine if you need another image.',
					'return_format' => 'array',
					'required'      => true,
					'wrapper'       => [ 'width' => 50 ],
				]
			),
			acf_group(
				[
					'label'        => 'Product intro',
					'name'         => 'product',
					'instructions' => 'Your product content with name, description, ranges, format. Retailers have a tab for it. Generic terms translation in "Options" page.',
					'sub_fields'   => [
						acf_textarea(
							[
								'label'        => 'Short description',
								'name'         => 'desc-short',
								'rows'         => '2',
								'new_lines'    => 'br',
								'instructions' => htmlentities('This is for the listing product display, just a few words introducing the product.'),
							]
						),
						acf_textarea(
							[
								'label'        => 'Description heading',
								'name'         => 'desc-highlights',
								'rows'         => '3',
								'new_lines'    => 'br',
								'instructions' => htmlentities('This is for the listing product display, just a few words introducing the product in pink.'),
								'wrapper'      => [ 'width' => 50 ],
							]
						),
						acf_textarea(
							[
								'label'        => 'Description',
								'name'         => 'desc-other',
								'rows'         => '3',
								'new_lines'    => 'br',
								'instructions' => htmlentities('Second part to introduce the product.'),
								'wrapper'      => [ 'width' => 50 ],
							]
						),
					],
				]
			),
			acf_repeater(
				[
					'label'        => 'Format available',
					'name'         => 'size-list',
					'layout'       => 'table',
					'instructions' => 'Add pack size available. E.G.: 120ml.',
					'wrapper'      => [ 'width' => 50 ],
					'sub_fields'   => [
						acf_text(
							[
								'label' => 'Product size',
								'name'  => 'size',
							]
						),
					],
				]
			),
			acf_repeater(
				[
					'label'        => 'Icons',
					'name'         => 'icon-list',
					'layout'       => 'table',
					'instructions' => 'Add icons of singularity. Please prefer square format and svg icon.',
					'wrapper'      => [ 'width' => 50 ],
					'sub_fields'   => [
						acf_image(
							[
								'label' => 'Icon',
								'name'  => 'icon',
								'return_format' => 'array',
							]
						),
					],
				]
			),
			acf_tab(
				[
					'label' => 'Retailers',
					'name'  => 'retailers_tab',
				]
			),
			acf_repeater(
				[
					'label'        => 'Retailers',
					'name'         => 'retailers',
					'layout'       => 'table',
					'instructions' => 'Custom retailers for this product if needed. Insert the logo, link and google ID tracking if needed.',
					'sub_fields'   => [
						acf_image(
							[
								'label'         => 'Logo',
								'name'          => 'logo',
								'instructions'  => 'Please use format 148x80px.',
								'return_format' => 'array',
								'required'      => true,
								'wrapper'       => [ 'width' => 33 ],
							]
						),
						acf_link(
							[
								'label'         => 'Retailer link',
								'name'          => 'link',
								'instructions'  => htmlentities('Add retailer\'s name in Link text box.') ,
								'required'      => true,
								'wrapper'       => [ 'width' => 33 ],
							]
						),
						acf_text(
							[
								'label'   => 'Google tag ID',
								'name'    => 'id',
								'wrapper' => [ 'width' => 33 ],
							]
						),
					],
				]
			),
			acf_tab(
				[
					'label' => 'Informations',
					'name'  => 'informations_tab',
				]
			),
			acf_repeater(
				[
					'label'        => 'Informations',
					'name'         => 'informations',
					'layout'       => 'block',
					'instructions' => 'Leave it empty to hide the section.',
					'sub_fields'   => [
						acf_true_false(
							[
								'label'         => 'Display type',
								'name'          => 'display',
								'instructions'  => 'Choose if it is a tab or an external link.',
								'ui'            => 1,
								'ui_on_text'    => 'Link',
								'ui_off_text'   => 'Tab',
								'wrapper'       => [ 'width' => 40 ],
							]
						),
						acf_text(
							[
								'label'             => 'Title',
								'name'              => 'title',
								'instructions'      => 'The title of the tabs. Leave it empty if this tabs is a link.',
								'wrapper'           => [ 'width' => 60 ],
								'conditional_logic' => [ [ acf_conditional( 'display', 0 ) ] ]
							]
						),
						acf_link(
							[
								'label'        => 'External link',
								'name'         => 'link',
								'instructions' => 'If needed choose a external link or let it empty.' ,
								'wrapper'      => [ 'width' => 60 ],
								'conditional_logic' => [ [ acf_conditional( 'display', 1 ) ] ]
							]
						),
						acf_wysiwyg(
							[
								'label'        => 'Content',
								'name'         => 'content',
								'instructions' => 'Content of the tabs.',
								'toolbar'      => 'basic',
								'media_upload' => false,
								'conditional_logic' => [ [ acf_conditional( 'display', 0 ) ] ]
							]
						),
					],
				]
			),
			acf_tab(
				[
					'label' => 'Patchwork',
					'name'  => 'patchwork_tab',
				]
			),
			acf_group(
				[
					'label'        => '',
					'name'         => 'patchwork',
					'sub_fields'   => [
						acf_true_false(
							[
								'label'         => 'Display patchwork section',
								'name'          => 'display',
								'instructions'  => 'Do yo need an extra section? Select "Yes" if you do.',
								'ui'            => 1,
								'ui_on_text'    => 'Yes please',
								'ui_off_text'   => 'No, thanks',
							]
						),
						acf_group(
							[
								'label'        => 'Left',
								'name'         => 'left',
								'wrapper'      => [ 'width' => 55 ],
								'conditional_logic' => [ [ acf_conditional( 'display', 1 ) ] ],
								'sub_fields'   => [
									acf_image(
										[
											'label'         => 'Large image',
											'name'          => 'large_image',
											'instructions'  => 'Here is the product working picture.',
											'return_format' => 'array',
										]
									),
									acf_link(
										[
											'label'         => 'Link',
											'name'          => 'link',
											'instructions'  => 'Link to the section you want.' ,
											'wrapper'       => [ 'width' => 50 ],
										]
									),
									acf_image(
										[
											'label'         => 'Small image',
											'name'          => 'small_image',
											'instructions'  => 'Small illustration image.',
											'return_format' => 'array',
											'wrapper'       => [ 'width' => 50 ],
										]
									),
								]
							]
						),
						acf_group(
							[
								'label'        => 'Right',
								'name'         => 'right',
								'wrapper'      => [ 'width' => 45 ],
								'conditional_logic' => [ [ acf_conditional( 'display', 1 ) ] ],
								'sub_fields'   => [
									acf_wysiwyg(
										[
											'label'         => 'Title',
											'name'          => 'title',
											'instructions'  => 'Title of the right pink section.',
											'toolbar'       => 'basic',
											'media_upload'  => false,
											'default_value' => 'Bronchostop® relieves dry + chesty coughs',
											'conditional_logic' => [ [ acf_conditional( 'display', 1 ) ] ]
										]
									),
									acf_link(
										[
											'label'         => 'Link',
											'name'          => 'link',
											'instructions'  => 'Link to the section you want.' ,
											'wrapper'       => [ 'width' => 50 ],
										]
									),
								]
							]
						)
					],
				]
			),
			acf_tab(
				[
					'label' => 'Extra',
					'name'  => 'extra_tab',
				]
			),
			acf_group(
				[
					'label'        => '',
					'name'         => 'extra',
					'sub_fields'   => [
						acf_true_false(
							[
								'label'         => 'Display extra section',
								'name'          => 'display',
								'instructions'  => 'Do yo need an extra section? Select "Yes" if you do.',
								'ui'            => 1,
								'ui_on_text'    => 'Yes please',
								'ui_off_text'   => 'No, thanks',
								'wrapper'       => [ 'width' => 40 ],
							]
						),
						acf_textarea(
							[
								'label'         => 'Title',
								'name'          => 'title',
								'instructions'  => 'Title of the last section.',
								'rows'          => '1',
								'new_lines'     => 'br',
								'default_value' => 'No need to worry about what type of cough you are experiencing',
								'wrapper'       => [ 'width' => 60 ],
								'conditional_logic' => [ [ acf_conditional( 'display', 1 ) ] ]
							]
						),
						acf_wysiwyg(
							[
								'label'         => 'Content',
								'name'          => 'content',
								'instructions'  => 'Content of the last section.',
								'toolbar'       => 'basic',
								'media_upload'  => false,
								'default_value' => 'Thanks to the action of both its ingredients, Bronchostop Syrup is suitable for any cough. Thyme herb extract acts as an expectorant, helping you to eliminate secretions that block your airways, while marshmallow root extract provides your throat with a protective layer to soothe the irritation. Suitable for adults and kids over 12 year.',
								'conditional_logic' => [ [ acf_conditional( 'display', 1 ) ] ]
							]
						),
					],
				]
			),
			acf_tab(
				[
					'label' => 'Related products',
					'name'  => 'related_tab',
				]
			),
			acf_group(
				[
					'label'        => '',
					'name'         => 'related',
					'sub_fields'   => [
						acf_textarea(
							[
								'label'         => 'Title',
								'name'          => 'title',
								'rows'          => '1',
								'instructions'  => 'Title of related products.',
								'placeholder'   => 'Your solution to relieve any cough',
								'default_value' => 'Your solution to relieve any cough',
								'required'      => true,
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_link(
							[
								'label'         => 'Link',
								'name'          => 'link',
								'instructions'  => 'Link to all products.' ,
								'required'      => true,
								'wrapper'       => [ 'width' => 50 ],
							]
						),
						acf_relationship(
							[
								'label'         => 'Select products to display',
								'name'          => 'selection',
								'post_type'     => [ 'product' ],
								'filters'       => false,
								'elements'      => [ 'featured_image' ],
								'required'      => false,
								'instructions'  => 'Select a product to display as a related product on the bottom of the page. Leave it empty to hide the section.',
								'return_format' => 'object',
							]
						),
					]
				]
			),
		];
		return $base_fields;
	}
	
	private function set_location() {
		$location = [
			[
				acf_location( 'post_type', '==', 'product' ),
			],
		];
		return $location;
	}
}

$acf_product = new InitAcfProduct();
$acf_product->init();