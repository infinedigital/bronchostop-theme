<?php
// Generic text fields
$generic   = get_field( 'generic', 'options' );

// Fields
$pack      = get_field( 'pack' );
$product   = get_field( 'product' );
$extra     = get_field( 'extra' );
$patchwork = get_field( 'patchwork' );

$modal = [];
$e     = 0;
?>

<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<?php 
				if( $pack ) : 
					$img    = $pack;
					$alt    = $img[ 'alt' ];
					$large  = $img[ 'sizes' ][ 'large' ];
					$small  = $img[ 'sizes' ][ 'large' ];
					$height = $img[ 'sizes' ][ 'large-height' ];
					$width  = $img[ 'sizes' ][ 'large-width' ];
				?>
				<picture>
					<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
					<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
				</picture>
				<?php endif; ?>
			</div>
			<div class="col">
				<div>
					<h1><?php the_title(); ?></h1>
					<ul>
						<?php 
						$terms = get_terms( 'product-category', array( 'hide_empty' => 0, 'parent' =>0 ) ); 
						foreach( $terms as $term ) :
							$link = get_term_link( $term->slug, $term->taxonomy );
							$name = $term->name;
						?>
						<li>
							<a href="<?php echo $link; ?>" class="" title="<?php echo $name; ?>"><?php echo $name; ?></a>
						</li>
						<?php endforeach; ?>
					</ul>
					<?php if( have_rows( 'size-list' ) ) : ?>
					<div>
						<?php echo $generic[ 'pack_size' ]; ?>
						<ul>
							<?php while( have_rows( 'size-list' ) ) : the_row(); ?>
							<li><?php the_sub_field( 'size' ); ?></li>
							<?php endwhile; ?>
						</ul>
					</div>
					<?php endif; ?>
				</div>
				<div>
					<h2>
						<?php echo $product[ 'desc-highlights' ]; ?>
					</h2>
					<h3>
						<?php echo $product[ 'desc-other' ]; ?>
					</h3>
					<?php if( have_rows( 'icon-list' ) ) : ?>
					<div>
						<?php
						while( have_rows( 'icon-list' ) ) : the_row();
							$img    = get_sub_field( 'icon' );
							$alt    = $img[ 'alt' ];
							$url    = $img[ 'url' ];
							$height = $img[ 'sizes' ][ 'large-height' ];
							$width  = $img[ 'sizes' ][ 'large-width' ];
						?>
						<picture>
							<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" />
						</picture>
						<?php endwhile; ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<?php if( have_rows( 'retailers' ) ) : ?>
		<div class="row">
			<div class="col-12">
				<ul>
					<?php
					while( have_rows( 'retailers' ) ) : the_row();
						$link   = get_sub_field( 'link' );
						$id     = get_sub_field( 'id' );
						$img    = get_sub_field( 'logo' );
						$alt    = $img[ 'alt' ];
						$url    = $img[ 'url' ];
						$height = $img[ 'sizes' ][ 'large-height' ];
						$width  = $img[ 'sizes' ][ 'large-width' ];
					?>
					<li>
						<a id="<?php echo $id; ?>" href="<?php echo $link[ 'url' ]; ?>" class="" title="<?php echo $link[ 'title' ]; ?>">
							<picture>
								<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $generic[ 'buy_now' ]; ?>" />
							</picture>
							<button class="btn btn-outline">
								<?php echo $generic[ 'buy_now' ]; ?>
							</button>
						</a>
					</li>
					<?php endwhile; ?>
				</ul>
			</div>
		</div>
		<?php endif; ?>
		<?php if( have_rows( 'informations' ) ) : $i = 0; ?>
		<div class="row">
			<div class="col">
				<div class="tabs">
					<nav class="tabs-nav">
						<?php
						while( have_rows( 'informations' ) ) : the_row();
							$display = get_sub_field( 'display' );
							$title   = get_sub_field( 'title' );
							$ext     = get_sub_field( 'link' );
							
							if( $display == 0 ) :
						?> 
						<span class="tabs-link" data-target="#tabs-<?php echo $i; ?>">
							<?php echo $title; ?> 
						</span>
						<?php else : ?> 
						<a href="<?php echo $ext[ 'url' ]; ?>" class="tabs-link tabs-extlink" title="<?php echo $ext[ 'title' ]; ?>">
							<?php echo $ext[ 'title' ]; ?> 
						</a>
						<?php
							endif; $i++;
						endwhile; $i = 0;
						?> 
					</nav>
					<div class="tabs-planels">
						<?php
						while( have_rows( 'informations' ) ) : the_row();
							$display = get_sub_field( 'display' );
							$content = get_sub_field( 'content' );
							
							if( $display == 0 ) :
						?> 
						<div id="tabs-<?php echo $i; ?>" class="tabs-panel">
							<div class="tabs-inner custom-wysiwyg">
								<?php echo $content; ?>
							</div>
						</div>
						<?php
							endif; $i++;
						endwhile; $i = 0;
						?> 
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section>

<?php 
if( $patchwork[ 'display' ] == 1 ) : 
	$left  = $patchwork[ 'left' ];
	$right = $patchwork[ 'right' ];

	$left_link = $left[ 'link' ];
	$left_image_large = $left[ 'large_image' ];
	$left_image_small = $left[ 'small_image' ];

	$right_title = preg_replace( "/\r|\n/", "", $right[ 'title' ]);
	$right_link  = $right[ 'link' ];
?>
<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<?php 
				if( $left_image_large ) : 
					$img    = $left_image_large;
					$alt    = $img[ 'alt' ];
					$large  = $img[ 'sizes' ][ 'large' ];
					$small  = $img[ 'sizes' ][ 'large' ];
					$height = $img[ 'sizes' ][ 'large-height' ];
					$width  = $img[ 'sizes' ][ 'large-width' ];
				?> 
				<div class="row">
					<div class="col-12">
						<picture>
							<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
							<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" /> 
						</picture>
					</div>
				</div>
				<?php endif; ?>
				<div class="row">
					<?php if( $left_link ) : ?> 
					<div class="col">
						<a href="<?php echo $left_link[ 'url' ]; ?>" title="<?php echo $left_link[ 'title' ]; ?>">
							<?php echo $left_link[ 'title' ]; ?>
						</a>
					</div>
					<?php endif; ?>
					<?php 
					if( $left_image_small ) : 
						$img    = $left_image_small;
						$alt    = $img[ 'alt' ];
						$large  = $img[ 'sizes' ][ 'large' ];
						$small  = $img[ 'sizes' ][ 'large' ];
						$height = $img[ 'sizes' ][ 'large-height' ];
						$width  = $img[ 'sizes' ][ 'large-width' ];
					?> 
					<div class="col">
						<picture>
							<source srcset="<?php echo $large; ?>" media="(min-width: 1024px)" type="image/jpeg">
							<img src="<?php echo $small; ?>" alt="<?php echo $alt; ?>" height="<?php echo $height; ?>" width="<?php echo $width; ?>" /> 
						</picture>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<?php if( $right_title ) : ?> 
			<div class="col">
				<h3>
					<a href="<?php echo $right_link[ 'url' ]; ?>" title="<?php echo strip_tags( $right_title ); ?>">
						<?php echo strip_tags( $right_title, '<strong><em><br>' ); ?> 
					</a>
				</h3>
				<a href="<?php echo $right_link[ 'url' ]; ?>" title="<?php echo $right_link[ 'title' ]; ?>">
					<?php echo $right_link[ 'title' ]; ?>
				</a>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if( $extra[ 'display' ] == 1 ) : ?>
<section>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>
					<?php echo $extra[ 'title' ] ?> 
				</h3>
				<div class="custom-wysiwyg">
					<?php echo $extra[ 'content' ] ?> 
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<?php get_template_part( 'templates/product', 'related' ); ?> 