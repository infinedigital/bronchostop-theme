<?php
/*
* Template Name: FAQ
*/

// Generic text fields
$generic = get_field( 'generic', 'options' );

// Fields
$faq = get_field( 'faq' );

$modal = [];
$e     = 0;
?>

<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php if( have_rows( 'faq' ) ) : $i = 0; ?> 
				<ul class="faq container-fluid">
					<?php
					while( have_rows( 'faq' ) ) : the_row();
						$title   = get_sub_field( 'title' );
						$content = get_sub_field( 'content' );
					?> 
					<li class="faq-item unstyled-list">
						<div class="faq-header">
							<?php if( $i == 0 ) : ?> 
							<h2 class="faq-title"><?php echo $title; ?></h2>
							<?php else : ?> 
							<h3 class="faq-title"><?php echo $title; ?></h3>
							<?php endif; ?> 
							<button class="faq-trigger"></button>
						</div>
						<div class="faq-content">
							<?php echo $content; ?>
						</div>
					</li>
					<?php $i++; endwhile; ?> 
				</ul>
				<?php endif; ?> 
			</div>
		</div>
	</div>
</section>

<?php get_template_part( 'templates/product', 'related' ); ?> 