<?php
// Generic text fields
$generic = get_field( 'generic', 'options' );

// Fields
$facts = get_field( 'module_facts', 'options' );
?>

<?php
if( have_rows( 'module_facts', 'options' ) ) : 
	while( have_rows( 'module_facts', 'options' ) ) : the_row();
		if( have_rows( 'facts' ) ) : 
?> 
<div class="facts">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="facts-header">
					<div class="facts-title"><?php echo $facts[ 'title' ]; ?></div>
				</div>
				<div class="facts-slider">
					<?php 
					while( have_rows( 'facts' ) ) : the_row(); 
						$title   = get_sub_field( 'title' );
						$content = get_sub_field( 'content' );
					?> 
						<div class="fact">
							<h4 class="fact-title">
								<?php echo $title; ?>
							</h4>
							<div class="fact-content">
								<?php echo $content; ?>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
				<div class="facts-slider-nav">
					<button class="facts-slider-btn facts-slider-prev">
						<?php echo $facts[ 'prev' ]; ?> 
					</button>
					<button class="facts-slider-btn facts-slider-next">
						<?php echo $facts[ 'next' ]; ?> 
					</button>
				</div>
				<div class="facts-button">
					<a href="<?php echo $facts[ 'btn' ][ 'url' ]; ?>" class="btn" title="<?php echo $facts[ 'btn' ][ 'title' ]; ?>">
						<?php echo $facts[ 'btn' ][ 'title' ]; ?> 
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
		endif;
	endwhile;
endif;
?>