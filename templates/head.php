<?php header( 'X-UA-Compatible: IE=edge' ); ?>
<!doctype html>
<?php if ( stripos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 10' ) ) : ?>
<html class="ie ie10" <?php language_attributes(); ?>>
<?php elseif ( stripos( $_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0' ) ) : ?>
<html class="ie ie11" <?php language_attributes(); ?>>
<?php elseif ( stripos( $_SERVER['HTTP_USER_AGENT'], 'Chrome' ) == false && stripos( $_SERVER['HTTP_USER_AGENT'], 'Safari' ) !== false ) : ?>
<html class="safari no-js" <?php language_attributes(); ?>>
<?php elseif ( stripos( $_SERVER['HTTP_USER_AGENT'], 'Firefox' ) ) : ?>
<html class="firefox no-js" <?php language_attributes(); ?>>
<?php else : ?>
<!--[if !IE]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<?php endif; ?>
<head>
	<meta charset="utf-8">
	<meta name="theme-color" content="#032B76">
	<link rel="manifest" href="/manifest.json">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<base href="<?php echo site_url(); ?>" />
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
</head>
