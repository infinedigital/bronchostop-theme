<?php
// Generic text fields
$generic = get_field( 'generic', 'options' );

// Fields
$reference = get_field( 'reference' );
$brand     = get_field( 'brand', 'options' );
$menu      = get_field( 'menu-pages', 'options' );
$buynow    = get_field( 'menu-buynow', 'options' );
$footer    = get_field( 'footer', 'options' );
$legal     = get_field( 'legal', 'options' );

// Languages
$lang = get_field( 'lang', 'options' );

// Active page
$active_page = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>

<header id="top" class="header">
	<div class="navbar">
		<div class="container">
			<div class="row">
				<div class="navbar-logo col">
					<a href="<?php bloginfo( 'url' ); ?>" title="<?php echo $brand[ 'slogan' ]; ?>">
						<?php if( $brand[ 'logo' ] ) : ?>
						<picture class="footer-logo-img">
							<img src="<?php echo $brand[ 'logo' ][ 'url' ]; ?>" alt="<?php echo $brand[ 'slogan' ]; ?>">
						</picture>
						<?php endif; ?>
					</a>
				</div>
				<nav class="navbar-menu col">
					<?php if( have_rows( 'menu-pages', 'options' ) ) : ?> 
					<ul class="navbar-nav-list">
						<?php
						while( have_rows( 'menu-pages', 'options' ) ) : the_row();
							$link = get_sub_field( 'link' );
						?> 
						<li class="navbar-nav-item<?php if( $link['url'] == $active_page ) { echo ' navbar-nav-item-active'; } ?> nosub">
							<a href="<?php echo $link[ 'url' ]; ?>" class="navbar-nav-link<?php if( $link['url'] == $active_page ) { echo ' navbar-nav-link-active'; } ?>" title="<?php echo $link[ 'title' ]; ?>">
								<?php echo $link[ 'title' ]; ?> 
							</a>
						</li>
						<?php endwhile; ?> 
						<?php
						if ($lang) :
							$translations = pll_the_languages( array( 'raw' => 1 ) );
						?>
						<li class="navbar-nav-item hassub">
							<div class="lang">
								<span class="lang-btn">
									<?php echo pll_current_language(); ?>
								</span>
								<ul class="lang-switch">
									<?php foreach ( $translations as $lang ) : ?>
									<li class="lang-item">
										<a href="<?php echo $lang[ 'url' ] ?>" class="lang-link" title="<?php echo $lang[ 'name' ] ?>">
											<?php echo $lang[ 'name' ] ?>
										</a>
									</li>
									<?php endforeach; ?>
								</ul>
							</div>
						</li>
						<?php endif; ?>
						<li class="navbar-nav-item nosub">
							<a href="<?php echo $buynow[ 'url' ]; ?> " class="btn btn-primary" title="<?php echo $buynow[ 'title' ]; ?> ">
								<?php echo $buynow[ 'title' ]; ?> 
							</a>
						</li>
					</ul>
					<?php endif; ?> 
				</nav>
				<nav class="navbar-trigger col">
					<button class="navbar-button"></button>
				</nav>
			</div>
		</div>
	</div>
	<div class="navscreen">
		<div class="navscreen">
			
		</div>
	</div>
</header>