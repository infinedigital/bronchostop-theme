<?php
// Generic text fields
$generic = get_field( 'generic', 'options' );
?>

<?php if( have_rows( 'retailers', 'options' ) ): ?>
<aside class="retailers">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php
				while ( have_rows( 'retailers', 'options' ) ) : the_row();
				$logo = get_sub_field( 'logo' );
				$link = get_sub_field( 'link' );
				$id   = get_sub_field( 'id' );
				?> 
				<a id="<?php echo $id; ?>" href="<?php echo $link[ 'url' ]; ?>" class="" title="<?php echo $link[ 'title' ]; ?>" target="_blank">
					<img src="<?php echo $logo[ 'url' ]; ?>" class="" alt="<?php echo $logo[ 'title' ]; ?>">
					<button class="btn btn-outline">
						<?php echo $generic[ 'buy_now' ]; ?> 
					</button>
				</a>
			<?php endwhile;  ?>
			</div>
		</div>
	</div>
</aside>
<?php endif; ?> 