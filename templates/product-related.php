<?php
// Generic text fields
$generic = get_field( 'generic', 'options' );

// Fields
$related  = get_field( 'related' );
$title    = $related[ 'title' ];
$link     = $related[ 'link' ];
$products = $related[ 'selection' ];
?>

<?php if( $products ) : ?>
<section>
	<div class="container">
		<div class="row">
			<div class="col">
				<h4><?php echo $title; ?></h4>
			</div>
			<div class="col">
				<a href="<?php echo $link[ 'url' ]; ?>" title="<?php echo $link[ 'title' ]; ?>">
					<?php echo $link[ 'title' ]; ?> 
				</a>
			</div>
		</div>
		<div class="row">
			<?php
			foreach( $products as $post ) :
				setup_postdata( $post );
				$pack    = get_field( 'pack' );
				$product = get_field( 'product' );
			?> 
			<div class="col">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<div>
						<?php the_post_thumbnail( 'large' ); ?>
					</div>
					<div>
						<div>
							<ul>
								<?php 
								$terms = get_terms( 'product-category', array( 'hide_empty' => 0, 'parent' =>0 ) ); 
								foreach( $terms as $term ) :
									$link = get_term_link( $term->slug, $term->taxonomy );
									$name = $term->name;
								?> 
								<li><?php echo $name; ?></li>
								<?php endforeach; ?> 
							</ul>
						</div>
						<h5><?php the_title(); ?></h5>
						<div><?php echo $product[ 'desc-short' ]; ?></div>
					</div>
					<div>
						<span>
							<?php echo $generic[ 'more_info' ]; ?> 
						</span>
					</div>
				</a>
			</div>
			<?php 
			endforeach; 
			wp_reset_postdata();
			?> 
		</div>
	</div>
</section>
<?php endif; ?>