<?php
// Generic text fields
$generic = get_field( 'generic', 'options' );

// Display options
$display   = get_field( 'footer_display' );
$cta       = $display[ 'display_cta' ];
$retailers = $display[ 'display_retailers' ];

// Fields
$reference = get_field( 'reference' );
$brand     = get_field( 'brand', 'options' );
$menu      = get_field( 'menu-pages', 'options' );
$buynow    = get_field( 'menu-buynow', 'options' );
$footer    = get_field( 'footer', 'options' );
$legal     = get_field( 'legal', 'options' );

// Languages
$lang = get_field( 'lang', 'options' );

// Active page
$active_page = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>

<footer class="footer">
	<div class="footer-cta">
		<?php 
		if( $cta == 1 ) :
			get_template_part( 'templates/retailers', 'cta' ); 
		endif;
		?> 
	</div>
	<div class="footer-retailers">
		<?php 
		if( $retailers == 1 ) :
			get_template_part( 'templates/retailers' ); 
		endif;
		?> 
	</div>
	<div class="footer-content">
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="footer-logo col">
						<a href="<?php bloginfo( 'url' ); ?>" title="<?php echo $brand[ 'slogan' ]; ?>">
							<?php if( $brand[ 'logo' ] ) : ?>
							<picture class="footer-logo-img">
								<img src="<?php echo $brand[ 'logo' ][ 'url' ]; ?>" alt="<?php echo $brand[ 'slogan' ]; ?>">
							</picture>
							<?php endif; ?>
							<?php if( $brand[ 'slogan' ] ) : ?>
							<h4 class="footer-baseline"><?php echo $brand[ 'slogan' ]; ?></h4>
							<?php endif; ?>
						</a>
					</div>
					<nav class="footer-nav col">
						<?php if( have_rows( 'menu-pages', 'options' ) ) : ?> 
						<ul class="footer-nav-list">
							<?php
							while( have_rows( 'menu-pages', 'options' ) ) : the_row();
								$link = get_sub_field( 'link' );
							?> 
							<li class="footer-nav-item<?php if( $link['url'] == $active_page ) { echo ' footer-nav-item-active'; } ?> nosub">
								<a href="<?php echo $link[ 'url' ]; ?>" class="footer-nav-link<?php if( $link['url'] == $active_page ) { echo ' footer-nav-link-active'; } ?>" title="<?php echo $link[ 'title' ]; ?>">
									<?php echo $link[ 'title' ]; ?> 
								</a>
							</li>
							<?php
							if ($lang) :
								$translations = pll_the_languages( array( 'raw' => 1 ) );
							?>
							<li class="footer-nav-item hassub">
								<div class="lang">
									<span class="lang-btn">
										<?php echo pll_current_language(); ?>
									</span>
									<ul class="lang-switch">
										<?php foreach ( $translations as $lang ) : ?>
										<li class="lang-item">
											<a href="<?php echo $lang[ 'url' ] ?>" class="lang-link" title="<?php echo $lang[ 'name' ] ?>">
												<?php echo $lang[ 'name' ] ?>
											</a>
										</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</li>
							<?php endif; ?>
							<?php endwhile; ?> 
							<li class="footer-nav-item nosub">
								<a href="<?php echo $buynow[ 'url' ]; ?> " class="btn btn-primary" title="<?php echo $buynow[ 'title' ]; ?> ">
									<?php echo $buynow[ 'title' ]; ?> 
								</a>
							</li>
						</ul>
						<?php endif; ?> 
					</nav>
				</div>
			</div>
		</div>
		
		<div class="footer-reference">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php
							if( $footer[ 'footnote' ] ) :
								echo '<div>' . $footer[ 'footnote' ] . '</div>';
							endif;
							?>
						<?php
						if( $reference ) :
							echo '<div>' . $reference . '</div>';
						endif;
						?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="footer-legal col">
						<div class="footer-copy">
							<p class="footer-copy-text">
								<?php echo $footer[ 'copy' ]; ?>
							</p>
							<ul class="footer-copy-list list-inline">
								<li class="list-inline-item">
									<a href="<?php echo $legal['cookies']['url']; ?>" target="_blank" title="<?php echo $legal['cookies']['title']; ?>">
										<?php echo $legal['cookies']['title']; ?>
									</a>
								</li>
								<li class="list-inline-item">
									<a href="<?php echo $legal['privacy']['url']; ?>" target="_blank" title="<?php echo $legal['privacy']['title']; ?>">
										<?php echo $legal['privacy']['title']; ?>
									</a>
								</li>
								<li class="list-inline-item">
									<a href="<?php echo $legal['terms']['url']; ?>" target="_blank" title="<?php echo $legal['terms']['title']; ?>">
										<?php echo $legal['terms']['title']; ?>
									</a>
								</li>
							</ul>
							<p class="footer-copy-address">
								<strong>Perrigo</strong> - <?php echo $brand[ 'address' ]; ?>
							</p>
							<p class="footer-copy-customer">
								<strong><?php echo $generic[ 'customer_service' ]; ?></strong> - 
								T: <?php echo $brand[ 'phone' ]; ?> 
								<?php if( $brand[ 'fax' ] ) : ?> - F: <?php echo $brand[ 'fax' ]; ?><?php endif; ?>
								<?php if( $brand[ 'mail' ] ) : ?> - <a href="<?php echo $brand[ 'mail' ]; ?>" title="<?php echo $generic[ 'customer_service' ]; ?>"><?php echo $brand[ 'mail' ]; ?></a><?php endif; ?>
							</p>
						</div>
					</div>
					<?php if( $footer[ 'perrigo' ] ) : ?>
					<div class="footer-perrigo col">
						<a href="#" title="<?php echo $footer[ 'perrigo' ][ 'title' ]; ?>">
							<img src="<?php echo $footer[ 'perrigo' ][ 'url' ]; ?>" class="logo-perrigo" alt="<?php echo $footer[ 'perrigo' ][ 'title' ]; ?>">
						</a>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</footer>