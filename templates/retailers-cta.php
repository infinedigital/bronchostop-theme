<?php
// Generic text fields
$generic = get_field( 'generic', 'options' );

// Fields
$cta  = get_field( 'cta', 'options' );
$link = $cta[ 'link' ];
?>

<aside class="retailers-cta">
	<div class="container">
		<div class="row">
			<div class="col">
				<h4 class="">
					<?php echo $cta[ 'title' ]; ?> 
				</h4>
				<h5 class="">
					<?php echo $cta[ 'content' ]; ?> 
				</h5>
				<a href="<?php echo $link[ 'url' ]; ?>" class="" title="<?php echo $link[ 'title' ]; ?>">
					<?php echo $link[ 'title' ]; ?> 
				</a>
			</div>
		</div>
	</div>
</aside>