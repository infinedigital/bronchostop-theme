<?php get_template_part( 'templates/header', 'page' ); ?>

<section class="container section-content ">
	<div class="row">
		<div class="col-md-10 offset-md-1 py-4 mt-4">
			<?php the_content(); ?>
		</div>
	</div>
</section>
